/**
 *
 * SportID Frontend client -- Environment config
 *
 * Copy this file to env.ts and fill out with your environment config
 *
 */

import { Env } from './env.interface';

export const env: Env = {

    /**
     * Base api url where to send requests
     * Must be defined without trailing slash
     */
    apiUrl: 'http://localhost:8000',

    /**
     * Default app name (e.g. used for page title)
     */
    appName: 'SportID',

    /**
     * GraphQL address where to send all graph requests
     */
    graphql: 'http://localhost:8000/graphql',

    /**
     * Locale value for currency pipe
     */
    currencyLocale: {
        currencyCode: 'EUR',
        locale: 'et-EE',
        localeFilePath: 'et',
    },

    onboarding: {
        EST: [
            'company',
            'serviceprovider',
            'eventorganizer',
        ],
        LVA: [
            'company',
            'serviceprovider',
        ],
        LTU: [
            'company',
            'serviceprovider',
        ],
        ARE: [
            'serviceprovider',
        ],
    },

    oauths: {
        facebook: {
            /*
             * Facebook client id for login with oauth.
             */
            clientId: 'example',

            /*
             * Facebook scope for login with oauth.
             */
            scope: ['email', 'public_profile', 'user_friends'],

            /*
             * Facebook url for login with oauth.
             */
            url: '/oauth/facebook',

            responseType: 'token',
        },
        google: {
            /*
             * Google client id for login with oauth.
             */
            clientId: 'example',

            /*
             * Google scope for login with oauth.
             */
            scope: ['profile', 'email'],

            /*
             * Google url for login with oauth.
             */
            url: '/oauth/google',

            responseType: 'token',
        },
    },

    /**
     * Allows to disable all external tracking either by boolean disabling all
     * tracking or adding full urls (regex can be used) to allow specific
     * domains disabled
     */

    // disableTracking: true,
    // or
    // disableTracking: ['https://www.sportid.com'],
};
