export interface Env {

    apiUrl: string;

    appName: string;

    graphql: string;

    currencyLocale: CurrencyLocaleInfo;

    onboarding: { [key: string]: string[] };

    oauths: Oauths;

    disableTracking?: boolean | string[];
}

export interface CurrencyLocaleInfo {
    currencyCode: string;
    locale: string;
    localeFilePath: string;
}

export interface EnvOauth {
    clientId: string;
    // ['profile', 'email']
    scope: string[];
    // /oauth/linkedin
    url: string;
    // token
    responseType?: string;
}

export interface Oauths {
    [name: string]: EnvOauth;
}

