(function(exports){
    "use strict";

    var path = require('path');

    var ROOT_FOLDER = path.resolve(__dirname, '..');

    function root(args) {
        args = Array.prototype.slice.call(arguments, 0);
        return path.join.apply(path, [ROOT_FOLDER].concat(args));
    }

    exports.root = root;

})(exports);
