const ENV  = process.env.ENV = process.env.NODE_ENV = 'development';

const helpers = require('../helpers');
const commonConfig = require('./webpack.conf.js');
const webpackMerge = require('webpack-merge');
const { AngularCompilerPlugin } = require('@ngtools/webpack');

module.exports = webpackMerge(commonConfig({env: ENV}), {

    mode: 'development',

    devtool: 'source-map',

    entry: {
        'main': './src/main.ts'
    },

    module: {
        rules: [
            {
                test: /\.ts$/,
                loader: '@ngtools/webpack'
            },
        ]
    },

    output: {
        path:              helpers.root('dist'),
        filename:          '[name].bundle.js',
        sourceMapFilename: '[name].map',
        chunkFilename:     '[id].chunk.js'
    },

    devServer: {
        port: 3000,
        host: 'localhost',
        historyApiFallback: true,
        watchOptions: {
            aggregateTimeout: 300,
            poll: 1000,
            ignored: /node_modules/,
        },
        stats: 'minimal',
    },

    performance: {
        hints: false
    },

    optimization: {
        removeAvailableModules: false,
    },

    plugins: [
        new AngularCompilerPlugin({
            tsConfigPath: helpers.root('tsconfig.json'),
            entryModule: helpers.root('src/app/app.module#AppModule'),
            skipCodeGeneration: true,
        }),
    ],
});
