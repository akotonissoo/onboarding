const ENV  = process.env.NODE_ENV = process.env.ENV = 'production';

const helpers      = require('../helpers');
const commonConfig = require('./webpack.conf.js');
const { AngularCompilerPlugin } = require('@ngtools/webpack');
const webpackMerge      = require('webpack-merge');
const CompressionPlugin = require('compression-webpack-plugin');
const OptimizeCSSAssetsPlugin = require("optimize-css-assets-webpack-plugin");

module.exports = webpackMerge(commonConfig({env: ENV}), {

    mode: 'production',

    entry: {
        'main': './src/main.ts'
    },

    module: {
        rules: [
            {
                test: /(?:\.ngfactory\.js|\.ngstyle\.js|\.ts)$/,
                loader: '@ngtools/webpack'
            },
        ]
    },

    output: {

        path: helpers.root('dist'),

        filename:          'scripts/[name].[chunkhash].bundle.js',
        sourceMapFilename: 'scripts/[name].[chunkhash].bundle.map',
        chunkFilename:     'scripts/[id].[chunkhash].chunk.js',

    },

    plugins: [
        new CompressionPlugin(),

        new AngularCompilerPlugin({
            tsConfigPath: helpers.root('tsconfig.json'),
            entryModule: helpers.root('src/app/app.module#AppModule'),
            sourceMap: false,
        }),

        new OptimizeCSSAssetsPlugin({
            cssProcessorOptions: {
                safe: true,
            },
        }),
    ],
});
