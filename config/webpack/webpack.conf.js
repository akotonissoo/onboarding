const webpack = require('webpack');
const helpers = require('../helpers');

// Webpack Plugins
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const CopyWebpackPlugin = require('copy-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const AssetsPlugin = require('assets-webpack-plugin');
const MomentLocalesPlugin = require('moment-locales-webpack-plugin');
const ProvidePlugin = webpack.ProvidePlugin;

const ContextReplacementPlugin = webpack.ContextReplacementPlugin;

const postCssOptions = {
    plugins: function() {
        return [
            require('autoprefixer')({
                overrideBrowserslist: ['last 2 versions', '>1%'],
                remove: false,
            }),
            require('postcss-discard-comments')()
        ];
    },
};


module.exports = function (options) {
    const isProd = options.env === 'production';

    return {
        entry: {
            'polyfills': './src/polyfills.ts',
        },

        resolve: {

            alias: {
                modernizr: helpers.root('config/.modernizrrc'),
                app: helpers.root('src/app')
            },
            extensions: ['.mjs', '.ts', '.js'],

            modules: [
                helpers.root('src'),
                helpers.root('node_modules')
            ]
        },

        module: {

            rules: [
                {
                    test:/\.html$/,
                    use: ['raw-loader']
                },

                {
                    test: /\.css$/,
                    use: [
                        'style-loader',
                        'css-loader'
                    ]
                },

                {
                    test: /\.(png|svg|jpe?g|gif)$/,
                    use: [
                        {
                            loader: 'file-loader',
                        },
                    ],
                },

                {
                    test: /\.component\.scss$/,
                    use: [
                        {
                            loader: 'postcss-loader',
                            options: postCssOptions,
                        },
                        {
                            loader: 'sass-loader',
                            options: {
                                prependData: '@import "global-styles";',
                                sassOptions: {
                                    includePaths: [ helpers.root('src/assets/scss/') ],
                                    outputStyle: isProd ? 'compressed' : 'nested',
                                }
                            }
                        }
                    ]
                },

                {
                    test: /\.font\.js$/,
                    use: [
                        'style-loader',
                        {
                            loader: 'css-loader',
                            options: {
                                importLoaders: 1,
                            }
                        },
                        'webfonts-loader',
                    ]
                },

                {
                    test: /\.file\.scss$/,
                    use: [
                        MiniCssExtractPlugin.loader,
                        {
                            loader: 'css-loader',
                            options: {
                                importLoaders: 1,
                            }
                        },
                        {
                            loader: "sass-loader",
                            options: {
                                prependData: '@import "global-styles";',
                                sassOptions: {
                                    includePaths: [ helpers.root('src/assets/scss/') ],
                                    outputStyle: isProd ? 'compressed' : 'nested',
                                }
                            }
                        },
                    ],
                },

                {
                    test: /\.svg$/,
                    use: [
                        {
                            loader: 'svg-url-loader',
                            options: {
                                limit: 10 * 1024,
                            }
                        },
                    ],
                },

                {
                    test: /\.modernizrrc\.js$/,
                    use: "modernizr-loader"
                },

                {
                    test: /\.modernizrrc(\.json)?$/,
                    use: ["modernizr-loader", "json-loader"]
                },

            ]

        },

        plugins: [

            /*
             * Description: Outputs file with bundle identifier and hashed name.
             * Useful for asking filenames from outside
             */
            new AssetsPlugin({
                path: helpers.root('dist'),
                filename: 'webpack-assets.json',
                prettyPrint: true
            }),

            /*
             * Description: Copy files and directories in webpack.
             * Copies project static assets.
             */
            new CopyWebpackPlugin([
                { from: 'src/assets', to: 'assets', ignore: ['scss/**/*'] },
                { from: 'src/favicon.ico', to: '' },
            ]),

            new ContextReplacementPlugin(
                // The (\\|\/) piece accounts for path separators in *nix and Windows
                /angular[\\/]core[\\/]@angular/,
                helpers.root('src') // location of your src
            ),

            new MomentLocalesPlugin({
                // 'en' is already included by default
                localesToKeep: ['et', 'ru', 'lt', 'lv', 'de', 'nl'],
            }),

            new ContextReplacementPlugin(
                /angular[\/\\]common[\/\\]locales$/, /\.\/(et|ar-AE|en-AE|en-US)\.js$/,
            ),

            /*
             * Description: Simplifies creation of HTML files to serve your webpack bundles.
             * This is especially useful for webpack bundles that include a hash in the filename
             * which changes every compilation.
             */
            new HtmlWebpackPlugin({
                template: 'src/index.html',
                chunksSortMode: function (a, b) {
                    const entryPoints = ['inline', 'polyfills', 'vendor', 'main'];
                    return entryPoints.indexOf(a.names[0]) - entryPoints.indexOf(b.names[0]);
                },
                minify: isProd ? {
                    caseSensitive: true,
                    collapseWhitespace: true,
                    keepClosingSlash: true,
                    removeComments: true,
                } : false
            }),

            new MiniCssExtractPlugin({
                // Options similar to the same options in webpackOptions.output
                // both options are optional
                filename:'stylesheets/[contenthash].css',
            }),

        ]
    };
};
