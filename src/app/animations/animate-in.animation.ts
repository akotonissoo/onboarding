import { animate, style, transition, trigger } from '@angular/animations';

export const animateIn =
    trigger('animateIn', [
        transition(':enter', [
            style({opacity: 0}),
            animate('100ms 100ms'),
        ]),
    ]);
