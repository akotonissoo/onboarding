import { Component, HostBinding, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { storageAllowed } from 'app/helpers';
import { LanguageService } from 'app/services';
import { CookieNoticeService } from './shared/cookie-notice';

@Component({
    selector: 'body', // tslint:disable-line
    templateUrl: './app.component.html',
})
export class AppComponent implements OnInit {
    @HostBinding('class') cssClass = 'ready';

    cookiesAllowed: boolean;
    notificationOptions = {
        maxStack: 2,
    };

    constructor(
        private languageService: LanguageService,
        private router: Router,
        private cookieNoticeService: CookieNoticeService,
    ) {
        this.cookiesAllowed = storageAllowed();
    }

    ngOnInit() {
        this.languageService.populateLangsForTranslateService();
        this.cookieNoticeService.showNotice();
    }

    onClose(): void {
        this.cookieNoticeService.hideNotice();
    }
}
