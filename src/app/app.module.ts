// Ordering imports breaks providing FactoriesModule services before ServicesModule services
// tslint:disable:ordered-imports
// angular
import { LOCALE_ID, NgModule } from '@angular/core';
import { registerLocaleData } from '@angular/common';
import { BrowserModule, HAMMER_GESTURE_CONFIG } from '@angular/platform-browser';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {
    DateAdapter,
    GestureConfig,
    MAT_DATE_FORMATS,
    MAT_DATE_LOCALE,
    MAT_LABEL_GLOBAL_OPTIONS,
} from '@angular/material/core';
import { MAT_FORM_FIELD_DEFAULT_OPTIONS } from '@angular/material/form-field';
import { MAT_AUTOCOMPLETE_DEFAULT_OPTIONS } from '@angular/material/autocomplete';
import { MAT_MOMENT_DATE_FORMATS, MatMomentDateModule, MomentDateAdapter } from '@angular/material-moment-adapter';
// external
import { TranslateLoader, TranslateModule, TranslateService } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import * as moment from 'moment';
// app
import { CookieNoticeModule } from './shared/cookie-notice';
import { DirectivesModule } from 'app/directives/directives.module';
import { FactoriesModule } from 'app/factories';
import { LoadingBarModule } from 'app/shared/loading-bar';
import { ModalModule } from 'app/shared/modal';
import { AppPreloader } from './app-preloader.module';
import { AppComponent } from './app.component';
import { ROUTES } from './app.routes';
import { ServicesModule } from './services';
import { env } from 'app/../../config/env';

require('normalize.css'); // tslint:disable-line
require('modernizr'); // tslint:disable-line
require('hammerjs'); // tslint:disable-line

export function createTranslateLoader(httpClient: HttpClient) {
    return new TranslateHttpLoader(httpClient, './assets/i18n/', '.json');
}

@NgModule({
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        HttpClientModule,

        ROUTES,

        DirectivesModule,
        FactoriesModule,
        ServicesModule,

        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: (createTranslateLoader),
                deps: [HttpClient],
            },
        }),
        LoadingBarModule.forRoot(),
        ModalModule.forRoot(),
        MatMomentDateModule,
        CookieNoticeModule.forRoot(),
    ],
    declarations: [ AppComponent ],
    bootstrap: [ AppComponent ],
    providers: [
        AppPreloader,
        {provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]},
        {provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS},
        {provide: MAT_LABEL_GLOBAL_OPTIONS, useValue: {float: 'always'}},
        {provide: MAT_FORM_FIELD_DEFAULT_OPTIONS, useValue: {appearance: 'legacy'}},
        {provide: HAMMER_GESTURE_CONFIG, useClass: GestureConfig},
        {provide: MAT_AUTOCOMPLETE_DEFAULT_OPTIONS, useValue: {autoActiveFirstOption: true}},
        {provide: LOCALE_ID, useValue: env.currencyLocale.locale},
    ],
})
export class AppModule {
    constructor(
        public translateService: TranslateService,
        private adapter: DateAdapter<any>,
    )  {
        import(
            /* webpackInclude: /(et|ar-AE)\.js$/ */
            `@angular/common/locales/${env.currencyLocale.localeFilePath}.js`).then(module => {
            return registerLocaleData(module.default);
        });

        translateService.onLangChange.subscribe((event: any) => {
            moment.locale(event.lang);
            this.adapter.setLocale(event.lang);
        });
    }
}
