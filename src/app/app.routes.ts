import { ModuleWithProviders } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AppPreloader } from './app-preloader.module';

export const routes: Routes = [
    {
        path: '',
        loadChildren: './landing/landing.module#LandingModule',
    },

    {
        path: '',
        loadChildren: './system/system.module#SystemModule',
        data: { preload: true },
    },

    {
        path: '**',
        redirectTo: '',
    },
];

export const ROUTES: ModuleWithProviders = RouterModule.forRoot(
    routes,
    {
        preloadingStrategy: AppPreloader,
        anchorScrolling: 'enabled',
    },
);
