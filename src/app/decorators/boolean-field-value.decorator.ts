function BooleanFieldValue() {
    return function BooleanFieldValueMetadata(target: any, key: string): void {
        const defaultValue = target[key];
        const localKey = `_${key}`;
        target[localKey] = defaultValue;

        Object.defineProperty(target, key, {
            get() {
                return (<any>this)[localKey]; // tslint:disable-line
            },
            set(value: boolean) {
                (<any>this)[localKey] = value != null && `${value}` !== 'false'; // tslint:disable-line
            },
        });
    };
}

export { BooleanFieldValue };
