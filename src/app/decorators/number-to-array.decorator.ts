function NumberToArray(offset: number = 0) {
    return function NumberToArrayMetadata(target: any, key: string): void {
        const defaultValue = target[key];
        const localKey = `_${key}`;
        target[localKey] = defaultValue;

        Object.defineProperty(target, key, {
            get() {
                return (<any>this)[localKey]; // tslint:disable-line
            },
            set(value: string | number) {
                if (typeof value === 'string') {
                    value = parseInt(value, 10);
                }
                (<any>this)[localKey] = Array(value).fill(0).map((x, i) => i + offset); // tslint:disable-line
            },
        });
    };
}

export { NumberToArray };
