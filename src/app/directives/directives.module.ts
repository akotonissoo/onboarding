import { NgModule } from '@angular/core';
import { DynamicAttributesDirective } from './dynamic-attributes';

@NgModule({
    exports: [DynamicAttributesDirective],
    declarations: [DynamicAttributesDirective],
})
export class DirectivesModule {}
