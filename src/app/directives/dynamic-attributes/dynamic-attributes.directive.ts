import { Directive, ElementRef, Inject, Input, OnInit } from '@angular/core';

@Directive({
    selector: '[sidDynamicAttributes]',
})
export class DynamicAttributesDirective implements OnInit {
    @Input() sidDynamicAttributes: {[key: string]: string | number};

    constructor(
        @Inject(ElementRef) public element: {nativeElement: Element},
    ) {}

    ngOnInit() {
        for (const key in this.sidDynamicAttributes) {
            if (this.sidDynamicAttributes.hasOwnProperty(key)) {
                this.element.nativeElement.setAttribute(key, this.sidDynamicAttributes[key] + '');
            }
        }
    }
}
