export class FieldsRequiredException extends Error {
    private static type: string = 'FieldsRequiredException';
    name: string = FieldsRequiredException.type;

    private static createError(requiredFields: string[]): string {
        let message = `${this.type}: Provide fields `;

        requiredFields.forEach(field => {
            message += `'${field}', `;
        });

        message.replace(/(?:, )([^, ]*)$/, '$1');

        return message;
    }

    constructor(requiredFields: string[]) {
        super(FieldsRequiredException.createError(requiredFields));
    }
}
