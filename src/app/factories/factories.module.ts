import { NgModule } from '@angular/core';

import { UsergroupFactory } from './usergroup';

@NgModule({
    providers: [
        UsergroupFactory,
    ],
})
export class FactoriesModule {}
