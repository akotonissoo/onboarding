import { Injectable } from '@angular/core';

import { Apollo } from 'apollo-angular';
import { ApolloQueryResult } from 'apollo-client';
import { ApolloService } from 'app/services';
import { Observable } from 'rxjs';
import * as queries from './usergroup.queries';

@Injectable()
export class UsergroupFactory {
    constructor(
        private apollo: Apollo,
    ) {}

    getCompanyRegistryInfo(name: string | Observable<string>): Observable<ApolloQueryResult<any>> {
        const variables = {
            name,
        };

        return ApolloService.fixWatchQuery(
            this.apollo.watchQuery({
                query: queries.companyRegistryInfoQuery,
                variables,
                fetchPolicy: 'network-only',
            }),
            variables,
        );
    }
}
