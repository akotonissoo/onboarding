import gql from 'graphql-tag';

export const companyRegistryInfoQuery = gql`
    query companyRegistryInfoQuery(
        $name: String,
    ) {
        info: company_registry_info(
            name: $name,
        ) {
            name
            vat_nr
            reg_nr
            address
            post_code
            exists
        }
    }
`;
