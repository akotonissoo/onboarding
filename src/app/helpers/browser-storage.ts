export function storageAllowed(): boolean {
    const storageTest = 'storageTest';
    try {
        localStorage.setItem(storageTest, storageTest);
        localStorage.removeItem(storageTest);
        return true;
    } catch (e) {
        console.warn('Storage not allowed, please allow cookies');
        return false;
    }
}
