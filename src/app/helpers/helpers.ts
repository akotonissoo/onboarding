export function isUndefined(elem: any): boolean {
    return typeof elem === 'undefined' || elem === null;
}

export function isObservable(elem: any): boolean {
    return elem && typeof elem === 'object' && typeof elem.subscribe === 'function';
}

export function selfOrParentHasClass(childElement: Element, className: string, recursive: boolean = false): boolean {

    const classList = childElement.className.split(' ');

    if (classList.indexOf(className) >= 0) {
        return true;
    }

    return parentHasClass(childElement, className, recursive);
}

export function parentHasClass(childElement: Element, className: string, recursive: boolean = false): boolean {

    if (!childElement.parentElement) {
        return false;
    }

    const parentClassList = childElement.parentElement.className.split(' ');
    const hasClassOnParent = parentClassList.indexOf(className) >= 0;

    if (!recursive) {
        return hasClassOnParent;
    }

    return hasClassOnParent || parentHasClass(childElement.parentElement, className, recursive);
}

export function selfOrParentIsEqualElement(
    childElement: Element,
    referenceElement: Element,
    recursive: boolean = false,
): boolean {

    if (childElement === referenceElement) {
        return true;
    }

    return parentIsEqualElement(childElement, referenceElement, recursive);
}

export function parentIsEqualElement(
    childElement: Element,
    referenceElement: Element,
    recursive: boolean = false,
): boolean {
    if (!childElement.parentElement) {
        return false;
    }

    const isEqualToParent = childElement.parentElement === referenceElement;

    if (!recursive) {
        return isEqualToParent;
    }

    return isEqualToParent || parentIsEqualElement(childElement.parentElement, referenceElement, recursive);
}

export function fixNewLines(desc: string): string[] {
    let description = desc.split('\n\n').filter(item => item);
    description = description.map(item => item.replace(/^\n|\n$/, '').replace(/\n/, '<br>'));
    return description;
}
