export function getPriorityCountries(): Set<string> {
    return new Set(['EST', 'EE', 'LVA', 'LV', 'LTU', 'LT']);
}

export function orderCountries(itemA: any, itemB: any): number {
    const priorityCountries = getPriorityCountries();

    if (priorityCountries.has(itemA.value) && !priorityCountries.has(itemB.value)) {
        return -1;
    }

    if (!priorityCountries.has(itemA.value) && priorityCountries.has(itemB.value)) {
        return 1;
    }

    if (itemA.label < itemB.label) {
        return -1;
    }
    if (itemA.label > itemB.label) {
        return 1;
    }
    return 0;
}
