export enum BrowserStorageTypeEnum {
    SessionStorage = 0,
    LocalStorage = 1,
}

export interface BrowserStorageValue {
    [key: string]: any;
    expiresAt?: number;
}
