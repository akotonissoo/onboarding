import { Observable } from 'rxjs';

export type AsObservables<T> = {
    [P in keyof T]?: Observable<T[P]>;
};

export type Never<T> = {
    [P in keyof T]?: never;
};

export type Omit<T, K extends keyof T> = Pick<T, Exclude<keyof T, K>>;
