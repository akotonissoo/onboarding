export enum PositionErrorsEnum {
    PermissionDenied = 0,
    PositionUnavailable = 1,
    Timeout = 2,
    UnsupportedBrowser = 3,
}
