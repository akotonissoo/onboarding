import { ApolloQueryResult } from 'apollo-client';

export interface QueryResult<T> extends ApolloQueryResult<T> {}
