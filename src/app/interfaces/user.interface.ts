export interface User {
    email: string;
    pin: string;
    profile: UserProfile;
    id?: number;
    hash?: string;
}

export interface UserProfile {
    first_name?: string;
    last_name?: string;
}

export enum UserProfileGender {
    Male = 'MALE' as any,
    Female = 'FEMA' as any,
}
