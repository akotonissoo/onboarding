import { Component, OnDestroy, OnInit } from '@angular/core';

@Component({
    templateUrl: './as-modal.component.html',
})
export class AsModalComponent implements OnInit, OnDestroy {

    ngOnInit() {
        (window as any).jQuery('#ldModalLogin').modal('show');
    }

    ngOnDestroy() {
        (window as any).jQuery('#ldModalLogin').modal('hide');
    }
}
