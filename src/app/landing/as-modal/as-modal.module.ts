import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';

import { PipesModule } from 'app/pipes';
import { ROUTES } from './as-modal.routes';

import { AsModalComponent } from './as-modal.component';
import { RegisterComponent } from './register';

@NgModule({
    imports: [ ROUTES, CommonModule, FormsModule, ReactiveFormsModule, TranslateModule, PipesModule ],
    declarations: [
        AsModalComponent,
        RegisterComponent,
    ],
})
export class AsModalModule {}
