import { ModuleWithProviders } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AsModalComponent } from './as-modal.component';

const routes: Routes = [
    {
        path: '',
        component: AsModalComponent,
    },
];

export const ROUTES: ModuleWithProviders = RouterModule.forChild(routes);
