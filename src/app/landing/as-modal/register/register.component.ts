import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import { TranslateService } from '@ngx-translate/core';
import { Subscription } from 'rxjs';

import { env } from 'app/../../config/env';
import { Oauths } from 'app/../../config/env.interface';
import { GroupTypeEnum } from 'app/public/onboarding/onboarding.interfaces';

@Component({
    selector: 'sid-register',
    templateUrl: './register.component.html',
})
export class RegisterComponent implements OnInit, OnDestroy {
    oauths: Oauths = env.oauths;
    registerForm: FormGroup;
    submitAttempted: boolean = false;

    private subscriptions: Subscription[] = [];

    constructor(
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private translateService: TranslateService,
    ) {}

    ngOnInit() {
        this.populateRegistrationForm();

        if (this.translateService.currentLang) {
            this.registerForm.get('language').patchValue(this.translateService.currentLang);

        }

        this.subscriptions.push(
            this.translateService.onLangChange.subscribe(() => {
                this.registerForm.get('language').patchValue(this.translateService.currentLang);
            }),
        );
    }

    ngOnDestroy() {
        this.subscriptions.forEach(subscription => subscription.unsubscribe());
    }

    handleRegister(): void {
        this.register();
    }

    onTermsChange(newValue: boolean): void {
        if (newValue === this.registerForm.get('acceptsPrivacy').value) {return; }
        this.registerForm.get('acceptsPrivacy').patchValue(newValue);
    }

    register() {
        this.submitAttempted = true;
        if (!this.registerForm.valid) {
            return;
        }

        this.registerForm.markAsPending();
        this.navigateToWizard(Math.random().toString(36).substr(2, 10));
    }

    private navigateToWizard(userHash: string, userRole: GroupTypeEnum = null): void {
        if (!userRole && this.activatedRoute.snapshot.queryParamMap.has('createGroup')) {
            userRole = this.activatedRoute.snapshot.queryParamMap.get('createGroup') as any;
        }

        this.router.navigate(
            ['wizard'],
            {
                queryParams: {
                    userId: userHash,
                    requireUserData: true,
                    createGroup: userRole,
                    selectedGroup: this.activatedRoute.snapshot.queryParamMap.get('selectedGroup'),
                    promo: this.activatedRoute.snapshot.queryParamMap.get('promo'),
                },
            },
        );
    }

    private populateRegistrationForm(): void {
        this.registerForm = new FormGroup({
            email: new FormControl(null, Validators.required),
            password: new FormControl(null, [Validators.required, Validators.minLength(8)]),
            language: new FormControl(null),
            acceptsTerms: new FormControl(false, Validators.requiredTrue),
            acceptsPrivacy: new FormControl(false, Validators.requiredTrue),
            registerToken: new FormControl(null),
        });

        this.subscriptions.push(
            this.registerForm.get('acceptsTerms').valueChanges.subscribe(value => this.onTermsChange(value)),
        );
    }
}
