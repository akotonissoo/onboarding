require('imports-loader?module=>undefined!./landing.vendor.js'); // tslint:disable-line

import { Component, OnDestroy, OnInit } from '@angular/core';

@Component({
    templateUrl: './landing.component.html',
    styleUrls: ['./landing.component.scss'],
})
export class LandingComponent implements OnInit, OnDestroy {
    ngOnInit() {
        document.querySelector('html').classList.add('landing');
    }

    ngOnDestroy() {
        document.querySelector('html').classList.remove('landing');
    }
}
