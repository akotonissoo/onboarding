import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';

import { PipesModule } from 'app/pipes';
import { ROUTES } from './landing.routes';
import { SharedModule } from './shared';

import { LandingComponent } from './landing.component';

@NgModule({
    imports: [ ROUTES, CommonModule, FormsModule, TranslateModule, SharedModule, PipesModule ],
    declarations: [
        LandingComponent,
    ],
})
export class LandingModule {}
