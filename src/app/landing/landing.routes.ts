import { ModuleWithProviders } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { LandingComponent } from './landing.component';

const routes: Routes = [
    {
        path: '',
        component: LandingComponent,
        children: [
            {
                path: '',
                loadChildren: './as-modal/as-modal.module#AsModalModule',
            },
        ],
    },
];

export const ROUTES: ModuleWithProviders = RouterModule.forChild(routes);
