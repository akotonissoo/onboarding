import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

import { LanguageService } from 'app/services';

@Component({
    selector: 'sid-modals',
    templateUrl: 'modals.component.html',
})
export class ModalsComponent implements OnInit {
    languages: any[] = [];
    selectedLanguage: string;

    constructor(
        private languageService: LanguageService,
        private translateService: TranslateService,
    ) {}

    ngOnInit() {
        this.initLanguages();
    }

    setLanguage(language: any): void {
        if (language && this.translateService.currentLang !== language.abbreviation) {
            this.selectedLanguage = language.default_name;
            this.translateService.use(language.abbreviation);
            this.languageService.setUserPreferredLanguage(language.abbreviation);
        }
    }

    private initLanguages(): void {
        this.selectedLanguage = this.translateService.currentLang;

        this.languages = [
            {abbreviation: 'en-us'},
            {abbreviation: 'et-ee'},
        ];

        const currentLangAbbr = this.translateService.currentLang;

        const foundLang = this.languages.find(language => language.abbreviation === currentLangAbbr);

        this.selectedLanguage = foundLang ? foundLang.default_name : this.selectedLanguage;
    }
}
