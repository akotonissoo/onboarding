import { NgModule } from '@angular/core';
import { ModalsModule } from './modals';

@NgModule({
    exports: [
        ModalsModule,
    ],
})
export class SharedModule {}
