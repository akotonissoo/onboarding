import { formatCurrency, getCurrencySymbol } from '@angular/common';
import { Pipe, PipeTransform } from '@angular/core';

import { env } from '../../../config/env';

@Pipe({name: 'sidCurrency'})
export class CurrencyPipe implements PipeTransform {
    transform(value: number, defaultValue: number = 0): string {
        if (!value) {
            value = defaultValue;
        }

        if (typeof value !== 'number') {
            value = parseFloat(value);
        }

        if (env.currencyLocale) {
            return formatCurrency(
                value,
                env.currencyLocale.locale,
                getCurrencySymbol(env.currencyLocale.currencyCode, 'wide', env.currencyLocale.locale),
            );
        }

        return formatCurrency(value, 'et-EE', getCurrencySymbol('EUR', 'wide', 'et-EE'));
    }
}
