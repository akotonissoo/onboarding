import { Pipe, PipeTransform } from '@angular/core';

import { fixNewLines } from 'app/helpers';

@Pipe({name: 'sidParagraphize'})
export class ParagraphizePipe implements PipeTransform {
    transform(value: string): string[] {
        return fixNewLines(value);
    }
}
