import { NgModule } from '@angular/core';

import { CurrencyPipe } from 'app/pipes/currency.pipe';
import { ParagraphizePipe } from 'app/pipes/paragraphize.pipe';

const PIPES = [
    CurrencyPipe,
    ParagraphizePipe,
];

@NgModule({
    exports: [ ...PIPES ],
    providers: [ ...PIPES ],
    declarations: [ ...PIPES ],
})
export class PipesModule {}
