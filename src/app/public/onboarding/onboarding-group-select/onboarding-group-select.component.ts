import { animate, group, query, stagger, style, transition, trigger } from '@angular/animations';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';

import { Subscription } from 'rxjs';

import { env } from 'app/../../config/env';
import { isUndefined } from 'app/helpers';
import { QueryParamsService } from 'app/services';
import { GroupInterface, GroupModalInterface, GroupTypeEnum } from '../onboarding.interfaces';

interface GroupInfo {
    [key: string]: GroupInterface;
}

interface GroupModals {
    [key: string]: GroupModalInterface;
}

@Component({
    selector: 'sid-onboarding-group-select',
    templateUrl: './onboarding-group-select.component.html',
    styleUrls: ['./onboarding-group-select.component.scss'],
    animations: [
        trigger('fade', [
            transition(':enter', [
                style({opacity: 0}),
                animate(600),
            ]),
            transition(':leave', [
                animate(600, style({opacity: 1})),
            ]),
        ]),
        trigger('groupsAnimation', [
            transition('* => *', [
                query(':leave', [
                    stagger(100, [
                        animate('500ms 200ms ease', style({
                            opacity: 0,
                        })),
                    ]),
                ], { optional: true }),
                query(':enter', [
                    style({
                        opacity: 0,
                        transform: 'translateY(-15%)',
                    }),
                    stagger(200, [
                        group([
                            animate(
                                '600ms 400ms ease',
                                style({
                                    opacity: 1,
                                }),
                            ),
                            animate(
                                '300ms 400ms ease',
                                style({
                                    transform: 'translateY(0)',
                                }),
                            ),
                        ]),
                    ]),
                ], { optional: true }),
            ]),
        ]),
    ],
})
export class OnboardingGroupSelectComponent implements OnInit, OnDestroy {
    createGroup: GroupTypeEnum;
    denyModalClose: boolean = false;
    groups: GroupInterface[] = [];
    groupModal: GroupModals = {
        [GroupTypeEnum.User]: {
            title: 'onboarding.groups.user.about_modal.title',
            imagePath: '/assets/images/illustrations/onboard_runner.svg',
            description: [
                {
                    title: 'onboarding.groups.user.about_modal.descriptions.title_1',
                    description: 'onboarding.groups.user.about_modal.descriptions.desc_1',
                },
                {
                    title: 'onboarding.groups.user.about_modal.descriptions.title_2',
                    description: 'onboarding.groups.user.about_modal.descriptions.desc_2',
                },
                {
                    title: 'onboarding.groups.user.about_modal.descriptions.title_3',
                    description: 'onboarding.groups.user.about_modal.descriptions.desc_3',
                },
            ],
        },
    };
    isModalOpen: boolean = false;
    requireUserData: boolean = false;
    selectedGroup: GroupTypeEnum = null;

    private groupInfo: GroupInfo = {
        [GroupTypeEnum.User]: {
            name: 'onboarding.groups.user.name',
            description: 'onboarding.groups.user.description',
            type: GroupTypeEnum.User,
            imagePath: '/assets/images/illustrations/onboard_regular.svg',
        },
    };
    private subscriptions: Subscription[] = [];

    constructor(
        private activatedRoute: ActivatedRoute,
        private queryParamsService: QueryParamsService,
        private router: Router,
    ) {}

    ngOnInit() {
        this.requireUserData = !!this.activatedRoute.snapshot.queryParamMap.get('requireUserData');
        this.populateGroups('EST');
        this.subscriptions.push(
            this.activatedRoute.queryParams.subscribe(params => this.onQueryParamsChange(params)),
        );
    }

    ngOnDestroy() {
        this.subscriptions.forEach(subscription => subscription.unsubscribe());
    }

    onGroupSelect(selectedGroup?: GroupInterface): void {
        let selectedGroupType = null;
        if (selectedGroup) {
            selectedGroupType = selectedGroup.type;
        }

        this.onModalOpenChange(!isUndefined(selectedGroupType));

        this.queryParamsService.updateQueryParams({
            selectedGroup: selectedGroupType,
        });
    }

    onGroupSelectAgreed(selectedGroup: GroupTypeEnum): void {
        // we make sure to close info modal about the current group
        this.onGroupSelect();

        this.queryParamsService.updateQueryParams({
            createGroup: selectedGroup,
        }, false);
    }

    onModalOpenChange(openStatus: boolean): void {
        this.isModalOpen = openStatus;
    }

    onUserSettingsUpdate(): void {
        this.requireUserData = false;
        this.queryParamsService.updateQueryParams({
            selectedGroup: GroupTypeEnum.User,
        });

        const selectedGroupType = GroupTypeEnum.User;
        this.onModalOpenChange(!isUndefined(selectedGroupType));
        this.queryParamsService.updateQueryParams({
            requireUserData: null,
        });
    }

    private handleGroupCreation(groupType: GroupTypeEnum | null): void {
        this.createGroup = groupType;

        if (groupType === null) {return; }

        this.denyModalClose = true;
        this.onModalOpenChange(true);
    }

    private onQueryParamsChange(params: Params): void {
        this.selectedGroup = null;

        if (!isUndefined(params.selectedGroup)) {
            this.selectedGroup = params.selectedGroup;
            this.onModalOpenChange(true);
        }

        let createGroup = null;

        if (!isUndefined(params.createGroup)) {
            createGroup = params.createGroup;
        }
        this.handleGroupCreation(createGroup);
    }

    private populateGroups(country: string): void {
        if (this.activatedRoute.snapshot.queryParamMap.has('userId')) {
            this.groups.push(this.groupInfo[GroupTypeEnum.User]);
        }

        env.onboarding[country].forEach(groupType => {
            this.groups.push(this.groupInfo[groupType]);
        });
    }
}
