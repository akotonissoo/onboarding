import { Component, EventEmitter, Input, Output } from '@angular/core';

import { Router } from '@angular/router';
import { animateIn } from '../../../../animations';
import { GroupModalInterface } from '../../onboarding.interfaces';

@Component({
    selector: 'sid-onboarding-info-modal',
    styleUrls: [ './onboarding-info-modal.component.scss' ],
    templateUrl: './onboarding-info-modal.component.html',
    animations: [animateIn],
})
export class OnboardingInfoModalComponent {
    @Input() info: GroupModalInterface;
    @Output() onCancel: EventEmitter<void> = new EventEmitter();
    @Output() onSubmit: EventEmitter<void> = new EventEmitter();
    constructor(
        private router: Router,
    ) {}


    async formSubmit(): Promise<void> {
        this.onSubmit.emit();
        await this.router.navigate(['/']);
    }
}
