import { Component, EventEmitter, OnDestroy, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

import { Subscription } from 'rxjs';

import * as moment from 'moment';
import { Moment } from 'moment';

import { Router } from '@angular/router';
import { animateIn } from 'app/animations';
import { UserProfileGender } from 'app/interfaces';
import { LocationService } from 'app/services';

@Component({
    selector: 'sid-onboarding-user-data-modal',
    styleUrls: ['./onboarding-user-data-modal.component.scss'],
    templateUrl: './onboarding-user-data-modal.component.html',
    animations: [animateIn],
})
export class OnboardingUserDataModalComponent implements OnInit, OnDestroy {
    @Output() onComplete: EventEmitter<void> = new EventEmitter<void>();

    birthDate: FormControl = new FormControl();
    countries: string[] = [];
    form: FormGroup;
    maxAllowedBirthYear: Moment = moment().subtract(16, 'years');
    minAllowedBirthYear: Moment = moment().subtract(99, 'years');
    submitAttempted: boolean = false;
    userProfileGender = UserProfileGender;

    private subscriptions: Subscription[] = [];

    constructor(
        private locationService: LocationService,
    ) {}

    ngOnInit() {
        this.populateForm();

        this.subscriptions.push(
            this.locationService.getCountries().subscribe(data => {
                this.countries = data.map(country => country.abbreviation);

                if (!this.form.get('countryAbbr').value) {
                    this.form.get('countryAbbr').patchValue(this.countries[0]);
                }
            }),
        );
    }

    ngOnDestroy() {
        this.subscriptions.forEach(subscription => subscription.unsubscribe());
    }

    formSubmit(): void {
        this.mapUserForSave();

        this.submitAttempted = true;

        if (!this.form.valid) {
            return;
        }

        this.form.markAsPending();
        this.onComplete.emit();
    }

    private checkBirthdayValidity(): void {
        if (moment(this.form.get('birthDate').value).isValid()) {
            this.form.get('birthDate').setErrors(null);
        } else {
            this.form.get('birthDate').setErrors({invalidDate: true});
        }
    }

    private populateForm(): void {
        this.form = new FormGroup({
            firstName: new FormControl(null, [Validators.required]),
            lastName: new FormControl(null, [Validators.required]),
            email: new FormControl(null, [Validators.required]),
            birthDate: new FormControl(null),
            gender: new FormControl(null),
            acceptsDirectMarketing: new FormControl(false),
            countryAbbr: new FormControl(null),
        });

        this.subscriptions.push(
            this.birthDate.valueChanges.subscribe(value => {
                return this.form.get('birthDate').patchValue(value.format('YYYY-MM-DD'));
            }),
        );
    }

    private mapUserForSave(): void {
        if (this.form.get('birthDate').value) {
            this.checkBirthdayValidity();
        }
    }
}
