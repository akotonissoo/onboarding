import { Component, HostListener, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
    selector: 'sid-onboarding',
    templateUrl: './onboarding.component.html',
    styleUrls: ['./onboarding.component.scss'],
    encapsulation: ViewEncapsulation.None,
})
export class OnboardingComponent implements OnInit, OnDestroy {
    ngOnInit() {
        document.querySelector('html').classList.add('onboarding');
    }

    ngOnDestroy() {
        document.querySelector('html').classList.remove('onboarding');
    }

    @HostListener('window:beforeunload', ['$event'])
    beforeClose($event) {
        $event.returnValue = true;
    }
}
