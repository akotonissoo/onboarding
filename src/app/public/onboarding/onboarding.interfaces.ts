export interface GroupInterface {
    name: string;
    description: string;
    type: GroupTypeEnum;
    imagePath: string;
}

export interface GroupModalInterface {
    title: string;
    imagePath: string;
    description: GroupModalDescInterface[];
}

export interface GroupModalDescInterface {
    title: string;
    description: string;
}

export enum GroupTypeEnum {
    User = 'user',
    Company = 'company',
    ServiceProvider = 'serviceprovider',
    EventOrganizer = 'eventorganizer',
}
