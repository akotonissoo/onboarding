import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { ROUTES } from './onboarding.routes';
import { SharedModule } from './shared';

import { PipesModule } from '../../pipes';
import {
    OnboardingGroupSelectComponent,
    OnboardingInfoModalComponent,
    OnboardingUserDataModalComponent,
} from './onboarding-group-select';
import { OnboardingComponent } from './onboarding.component';

@NgModule({
    imports: [ ROUTES, CommonModule, SharedModule, PipesModule ],
    declarations: [
        OnboardingComponent,
        OnboardingGroupSelectComponent,
        OnboardingInfoModalComponent,
        OnboardingUserDataModalComponent,
    ],
})
export class OnboardingModule {}
