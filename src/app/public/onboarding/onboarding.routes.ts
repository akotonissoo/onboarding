import { ModuleWithProviders } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { OnboardingGroupSelectComponent } from './onboarding-group-select';
import { OnboardingComponent } from './onboarding.component';

const routes: Routes = [
    {
        path: '',
        component: OnboardingComponent,
        children: [
            {
                path: '',
                component: OnboardingGroupSelectComponent,
            },
        ],
    },
];

export const ROUTES: ModuleWithProviders = RouterModule.forChild(routes);
