import { Component, EventEmitter, Input, Output } from '@angular/core';

import { GroupInterface } from '../../onboarding.interfaces';

@Component({
    selector: 'sid-group-box',
    styleUrls: [ './group-box.component.scss' ],
    templateUrl: './group-box.component.html',
})
export class GroupBoxComponent {
    @Input() group: GroupInterface;
    @Output() groupSelect: EventEmitter<GroupInterface> = new EventEmitter<GroupInterface>();

    onSelect(): void {
        this.groupSelect.emit(this.group);
    }
}
