import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { TranslateModule } from '@ngx-translate/core';

import { ButtonModule } from 'app/shared/button';
import { GroupBoxComponent } from './group-box.component';

@NgModule({
    imports: [ CommonModule, TranslateModule, ButtonModule ],
    declarations: [ GroupBoxComponent ],
    exports: [ GroupBoxComponent ],
})
export class GroupBoxModule {}
