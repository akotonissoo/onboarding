import { NgModule } from '@angular/core';

import { SharedModule as parentShared } from '../../shared';

import { GroupBoxModule } from './group-box';
import { StepIndicatorModule } from './step-indicator';

@NgModule({
    exports: [
        parentShared,
        GroupBoxModule,
        StepIndicatorModule,
    ],
})
export class SharedModule {}
