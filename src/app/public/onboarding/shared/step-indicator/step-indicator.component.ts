import { Component, Input } from '@angular/core';

import { NumberToArray } from 'app/decorators';

@Component({
    selector: 'sid-step-indicator',
    styleUrls: [ './step-indicator.component.scss' ],
    templateUrl: './step-indicator.component.html',
})
export class StepIndicatorComponent {
    @Input() selectedSteps: number;
    @Input() @NumberToArray() totalSteps: number;
}
