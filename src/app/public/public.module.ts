import { CommonModule } from '@angular/common';
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';

import { PublicComponent } from './public.component';
import { routes } from './public.routes';

import { SharedModule } from './shared';

@NgModule({
    schemas: [ CUSTOM_ELEMENTS_SCHEMA ],
    imports: [
        CommonModule,
        SharedModule,
        routes,
    ],
    declarations: [ PublicComponent ],
})
export class PublicModule {}
