import { RouterModule, Routes } from '@angular/router';

const ROUTES: Routes = [
    {
        path: 'wizard',
        loadChildren: './onboarding/onboarding.module#OnboardingModule',
    },
];

export const routes = RouterModule.forChild(ROUTES);
