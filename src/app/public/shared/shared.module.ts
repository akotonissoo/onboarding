import { NgModule } from '@angular/core';

import { SharedModule as parentShared } from '../../shared';

@NgModule({
    exports: [parentShared],
})
export class SharedModule {}
