import { Injectable } from '@angular/core';

import { QueryRef } from 'apollo-angular';
import { R } from 'apollo-angular/types';
import { ApolloQueryResult, OperationVariables } from 'apollo-client';
import { combineLatest, Observable, Observer, Subscription } from 'rxjs';

import { isObservable } from 'app/helpers';

@Injectable()
export class ApolloService {
    static fixWatchQuery<T, V = R>(
        queryRef: QueryRef<T>,
        variables: OperationVariables = {},
    ): Observable<ApolloQueryResult<T>> {
        return Observable.create((observer: Observer<ApolloQueryResult<T>>) => {
            let hadAllVars: boolean = false;
            let subscriptionUp: boolean = false;
            const subscriptions: Subscription[] = [];

            // we extract the observable variables

            const observableVariables = {};
            for (const key in variables) {
                if (variables.hasOwnProperty(key)) {
                    const variable = variables[key];
                    if (isObservable(variable)) {
                        observableVariables[key] = variable;
                    }
                }
            }
            const observableVariablesEntries = Object.entries(observableVariables);


            // In case we did not have any observables, we pass on all given variables

            if (!observableVariablesEntries.length) {
                queryRef.setVariables(variables);
                hadAllVars = true;
            } else {
                // In case we did have variables, we subscibe to all of them and pass to query on each change

                subscriptions.push(
                    combineLatest(
                        observableVariablesEntries.map(item => item[1]),
                    ).subscribe(results => {
                        hadAllVars = true;
                        const newVariables = {};
                        results.forEach((result, i) => {
                            newVariables[observableVariablesEntries[i][0]] = result;
                        });

                        for (const key in variables) {
                            if (variables.hasOwnProperty(key) && !isObservable(variables[key])) {
                                newVariables[key] = variables[key];
                            }
                        }

                        queryRef.setVariables(newVariables);
                        subscriptionUp = ApolloService.setupListerers(
                            queryRef,
                            observer,
                            hadAllVars,
                            subscriptionUp,
                            subscriptions,
                        );
                    }),
                );
            }

            subscriptionUp = ApolloService.setupListerers(
                queryRef,
                observer,
                hadAllVars,
                subscriptionUp,
                subscriptions,
            );

            return () => {
                subscriptions.forEach(subscription => subscription.unsubscribe());
            };
        });
    }

    private static setupListerers<T>(
        queryRef: QueryRef<T>,
        finalResult: Observer<ApolloQueryResult<T>>,
        hadAllVars: boolean,
        subscriptionUp: boolean,
        subscriptions = [],
    ): boolean {
        if (hadAllVars && !subscriptionUp) {
            subscriptions.push(queryRef.valueChanges.subscribe(
                result => {
                    finalResult.next(result);
                },
                error => {
                    finalResult.error(error);
                },
                () => {
                    finalResult.complete();
                },
            ));
            return true;
        }

        return subscriptionUp;
    }
}
