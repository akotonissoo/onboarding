import { Injectable } from '@angular/core';
import * as moment from 'moment';

import { BehaviorSubject, Observable } from 'rxjs';

import { storageAllowed } from 'app/helpers';
import { BrowserStorageTypeEnum, BrowserStorageValue } from 'app/interfaces';

interface SessionListeners {
    [name: string]: BehaviorSubject<string | BrowserStorageValue | null>;
}

@Injectable()
export class BrowserStorageService {
    private sessionListeners: SessionListeners = {};

    getItem(key: string, type?: BrowserStorageTypeEnum): string | BrowserStorageValue | null {
        if (!storageAllowed()) {return; }

        let value: any = this.getStorageObject(type).getItem(key);
        try {
            const parsedValue = JSON.parse(value);
            if (parsedValue && typeof parsedValue === 'object') {
                value = parsedValue;

                if (value.expiresAt && value.expiresAt < moment().unix()) {
                    this.removeItem(key, type);
                    return null;
                }
            }

        } catch {/**/}
        return value;
    }

    getItemObservable(key: string, type?: BrowserStorageTypeEnum): Observable<string | BrowserStorageValue | null> {
        return this.getSessionListener(key, type).asObservable();
    }

    removeItem(key: string, type?: BrowserStorageTypeEnum): void {
        if (!storageAllowed()) {return; }

        this.getStorageObject(type).removeItem(key);
    }

    setItem(key: string, value: string | BrowserStorageValue, type?: BrowserStorageTypeEnum): void {
        if (!storageAllowed()) {return; }

        if (typeof value !== 'string') {
            value = JSON.stringify(value);
        }

        this.getSessionListener(key, type).next(value);
        this.getStorageObject(type).setItem(key, value);
    }

    private createListenerKey(key: string, type?: BrowserStorageTypeEnum): string {
        if (!BrowserStorageTypeEnum.hasOwnProperty(type)) {
            type = BrowserStorageTypeEnum.SessionStorage;
        }
        return type + '_' + key;
    }

    private getSessionListener(
        key: string,
        type?: BrowserStorageTypeEnum,
    ): BehaviorSubject<string | BrowserStorageValue | null> {
        const listenerKey = this.createListenerKey(key, type);
        this.ensureItemObservable(listenerKey);
        return this.sessionListeners[listenerKey];
    }

    private getStorageObject(storageType: BrowserStorageTypeEnum): Storage {
        if (storageType === BrowserStorageTypeEnum.LocalStorage) {
            return localStorage;
        }

        return sessionStorage;
    }

    private ensureItemObservable(key: string, type?: BrowserStorageTypeEnum): void {
        if (!this.sessionListeners.hasOwnProperty(key)) {
            this.sessionListeners[key] = new BehaviorSubject(this.getItem(key));
        }
    }
}
