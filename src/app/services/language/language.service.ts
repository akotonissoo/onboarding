import { forwardRef, Inject, Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

import * as countries from 'i18n-iso-countries';

import { BrowserStorageTypeEnum } from 'app/interfaces';
import { BrowserStorageService } from 'app/services/browser-storage';
import { QueryParamsService } from 'app/services/query-params';

@Injectable()
export class LanguageService {

    private defaultLanguage = 'en-us';

    constructor(
        // tslint:disable:no-forward-ref
        @Inject(forwardRef(() => BrowserStorageService)) private browserStorageService: BrowserStorageService,
        @Inject(forwardRef(() => QueryParamsService)) private queryParamsService: QueryParamsService,
        private translateService: TranslateService,
    ) {
        this.translateService.onLangChange.subscribe(
            newLanguage => {
                const langParts = newLanguage.lang.split('-');

                const langAttr = document.createAttribute('lang');
                langAttr.value = langParts[0] + '-' + langParts[1].toUpperCase();
                document.getElementsByTagName('html')[0].attributes.setNamedItem(langAttr);
            },
        );
    }

    getUserPreferredLanguage(): string {
        const preferredLanguages = this.determinatePreferredLanguages();

        for (const preferredLanguage of preferredLanguages) {
            const closestAvailableLanguage = this.getClosestAvailableLanguage(preferredLanguage);

            if (closestAvailableLanguage) {
                return closestAvailableLanguage;
            }
        }

        return '';
    }

    populateLangsForTranslateService() {
        const langAbbrs = [
            'en-us',
            'et-ee',
        ];

        this.translateService.addLangs(langAbbrs);

        const preferredLanguage = this.getUserPreferredLanguage();
        this.translateService.setDefaultLang(this.defaultLanguage);
        this.translateService.use(preferredLanguage);

        this.loadCountryTranslations(langAbbrs);
    }

    setUserPreferredLanguage(language: string): void {
        this.browserStorageService.setItem('lang', language, BrowserStorageTypeEnum.LocalStorage);
    }

    private determinatePreferredLanguages(): string[] {
        const preferredLanguages: string[] = [];

        // Check, if already stored a langage
        const storageLanguage = this.browserStorageService.getItem('lang', BrowserStorageTypeEnum.LocalStorage) || '';
        if (storageLanguage && typeof storageLanguage === 'string') {
            preferredLanguages.push(storageLanguage);
        }

        // Check, if query params have a langage
        const langParam = new RegExp(/(?:lang=)(.+?)(?=&|$)/, 'gm').exec(window.location.search);

        if (langParam) {
            preferredLanguages.push(langParam[1]);
            // TODO: maybe not best place to do this
            this.queryParamsService.updateQueryParams({lang: null}, true);
        }

        // Domains should prefer their lang
        const domains = window.location.hostname.split('.');
        const topDomain = domains[domains.length - 1];
        const topDomainLang = this.mapTopDomainToLang(topDomain);
        if (topDomainLang) {
            preferredLanguages.push(topDomainLang);
        }

        // Get the preferences from user browser langauge preferences settings
        if (navigator.languages && navigator.languages.length) {
            preferredLanguages.push(...navigator.languages);
        }

        // If didn't support `navigator.languages`, fallback to browser language
        if (navigator.language) {
            preferredLanguages.push(navigator.language);
        }

        // If still don't have a language, use default
        preferredLanguages.push(this.defaultLanguage);

        return preferredLanguages;
    }

    private getClosestAvailableLanguage(language: string): string {
        const availableLangs = this.translateService.getLangs();

        return availableLangs.find(availableLang => {
            // If is exact match or different in case or different separator
            if (availableLang.toLowerCase() === language.toLowerCase() ||
                availableLang.toLowerCase() === language.replace(/_/, '-').toLowerCase()
            ) {
                return true;
            }

            // Check culture name match
            return availableLang.indexOf('-') !== -1 &&
                availableLang.split('-')[0].toLowerCase() === language.toLowerCase() ||
                availableLang.indexOf('_') !== -1 &&
                availableLang.split('_')[0].toLowerCase() === language.toLowerCase()
                ;
        });
    }

    private loadCountryTranslations(langAbbrs: string[]): void {
        langAbbrs.forEach(abbr => {
            try {
                abbr = abbr.split('-')[0];
                const language = require(`i18n-iso-countries/langs/${abbr}.json`);
                countries.registerLocale(language);
            } catch (e) {
                //
            }
        });
    }

    private mapTopDomainToLang(topDomain: string): string | null {
        // We ignore .com as that will be the default site and we set language
        // with user preferred language there
        switch (topDomain) {
            case 'dk':
                return 'de-de';
            case 'ee':
                return 'et-ee';
            case 'lt':
                return 'lt-lt';
            case 'lv':
                return 'lv-lv';
            case 'nl':
                return 'nl-nl';
            case 'ru':
                return 'ru-ru';
        }
        return null;
    }
}
