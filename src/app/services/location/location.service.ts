import { Injectable } from '@angular/core';
import { PositionErrorsEnum } from 'app/interfaces';
import { Observable, of, ReplaySubject } from 'rxjs';


@Injectable()
export class LocationService {
    private geolocationObservable: Observable<any>;
    private userLocation = new ReplaySubject<Position>(1);
    private userQueried: boolean = false;

    constructor() {
        this.createGeolocationObservable();
    }

    getCountries() {
        return of([
            {name: 'Estonia', alpha2: 'EE', abbreviation: 'EST'},
            {name: 'Latvia', alpha2: 'LV', abbreviation: 'LVA'},
        ]);
    }

    private createGeolocationObservable(): void {
        this.geolocationObservable = Observable.create(observer => {
            this.queryUserForLocation();

            this.userLocation.subscribe(
                userLocation => {
                    observer.next(userLocation);
                    observer.complete();
                },
                error => observer.error(error),
                () => observer.complete(),
            );
        });
    }

    private queryUserForLocation(): void {
        if (this.userQueried) {return; }

        this.userQueried = true;

        if (window.navigator && window.navigator.geolocation) {
            window.navigator.geolocation.getCurrentPosition(
                position => {
                    this.userLocation.next(position);
                    this.userLocation.complete();
                },
                error => {
                    this.userLocation.error(error);
                },
            );
        } else {
            this.userLocation.error({
                code: PositionErrorsEnum.UnsupportedBrowser,
            });
        }
    }
}
