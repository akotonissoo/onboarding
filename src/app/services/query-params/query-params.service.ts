import { Injectable } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';

import { filter } from 'rxjs/operators';

@Injectable()
export class QueryParamsService {

    private params: any = {};

    constructor(
        private activatedRoute: ActivatedRoute,
        private router: Router,
    ) {
        this.handleNavigationEnd(); // params needs to be populated on service init as well
        this.router.events.pipe(filter(result => result instanceof NavigationEnd)).subscribe(
            this.handleNavigationEnd.bind(this),
        );
    }

    mergeParams(fields: any = {}, update: boolean = false): any {
        const params = {};
        Object.assign(params, this.params);
        Object.assign(params, fields);
        if (update) {
            this.params = params;
        }
        return params;
    }

    updateQueryParams(fields: any, replaceUrl: boolean = true): void {
        const params = this.mergeParams(fields, true);
        this.router.navigate([], {
            relativeTo: this.activatedRoute,
            queryParams: params,
            replaceUrl,
        });
    }

    private handleNavigationEnd(): void {
        this.params = Object.assign({}, this.activatedRoute.snapshot.queryParams);
    }

}
