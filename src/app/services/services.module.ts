import { HttpClient } from '@angular/common/http';
import { APP_INITIALIZER, NgModule } from '@angular/core';

import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { Apollo, ApolloModule } from 'apollo-angular';
import { HttpLink, HttpLinkModule } from 'apollo-angular-link-http';

import { SidApolloClient } from './sid-apollo.service';

import { LoadingBarService } from '../shared/loading-bar';
import { BrowserStorageService } from './browser-storage';
import { LanguageService } from './language';
import { LocationService } from './location';
import { QueryParamsService } from './query-params';
import { UsergroupService } from './usergroup';

export function HttpLoaderFactory(httpClient: HttpClient) {
    return new TranslateHttpLoader(httpClient, './assets/i18n/', '.json');
}

@NgModule({
    imports: [
        ApolloModule,
        HttpLinkModule,
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: HttpLoaderFactory,
                deps: [HttpClient],
            },
        }),
    ],
    providers: [
        SidApolloClient,
        {
            provide: APP_INITIALIZER,
            useFactory: SidApolloClient.setupApollo,
            deps: [
                Apollo,
                HttpLink,
                LoadingBarService,
            ],
            multi: true,
        },
        BrowserStorageService,
        LanguageService,
        LocationService,
        QueryParamsService,
        UsergroupService,
    ],
    exports: [TranslateModule],
})
export class ServicesModule {}
