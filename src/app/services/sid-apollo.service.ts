import { Injectable } from '@angular/core';

import { Apollo } from 'apollo-angular';
import { HttpLink } from 'apollo-angular-link-http';
import { defaultDataIdFromObject, InMemoryCache } from 'apollo-cache-inmemory';
import { ApolloLink, RequestHandler } from 'apollo-link';
import { onError } from 'apollo-link-error';

import { LoadingBarService } from 'app/shared/loading-bar';

import { env } from '../../../config/env';

@Injectable()
export class SidApolloClient {
    private static apollo: Apollo;
    private static cache;
    private static hasApolloClient: boolean = false;

    static setupApollo(
        apollo: Apollo,
        httpLink: HttpLink,
        loadingService: LoadingBarService,
    ) {
        return () => {
            if (!SidApolloClient.hasApolloClient) {
                SidApolloClient.populateCache();
                SidApolloClient.apollo = apollo;
                const createdHttpLink = httpLink.create({
                    uri: env.graphql,
                    withCredentials: true,
                });

                const middlewareLink = new ApolloLink(SidApolloClient.createLoadingMiddleWare(loadingService));
                const afterwareLink = SidApolloClient.createLoadingErrorAfterware(loadingService);

                let link: any = middlewareLink.concat(createdHttpLink);
                link = afterwareLink.concat(link);

                apollo.create({
                    link,
                    cache: SidApolloClient.cache,
                });


                SidApolloClient.hasApolloClient = true;
            }
        };
    }

    private static createLoadingMiddleWare(loadingService: LoadingBarService): RequestHandler {
        return (operation, forward) => {
            if (operation.variables && operation.variables.ignoreLoadingBar) {
                return forward(operation);
            }

            loadingService.start(1);

            return forward(operation).map(response => {
                loadingService.finish(1);

                return response;
            });
        };
    }

    private static createLoadingErrorAfterware(loadingService: LoadingBarService) {
        return onError(() => {
            loadingService.finish(1);
        });
    }

    private static populateCache(): void {
        if (SidApolloClient.cache) {return; }

        const cacheOptions = {
            dataIdFromObject: object => {
                switch (object.__typename) {
                    case 'PurchaselogTransaction':
                        if (object.id === 0) {
                            return null;
                        }
                        return defaultDataIdFromObject(object);
                    case 'BusinessPackageWithDates':
                        return defaultDataIdFromObject(object.id + object.end_at);
                    default:
                        return defaultDataIdFromObject(object);
                }
            },
        };

        SidApolloClient.cache = new InMemoryCache(cacheOptions);
    }

    clearCache(): Promise<any> {
        return SidApolloClient.apollo.getClient().resetStore();
    }
}
