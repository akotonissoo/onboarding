import { Injectable } from '@angular/core';

import { Observable, of } from 'rxjs';

import { QueryResult } from 'app/interfaces';
import { UsergroupFactory } from '../../factories/usergroup';

@Injectable()
export class UsergroupService {

    constructor(
        private usergroupFactory: UsergroupFactory,
    ) {}

    getCompanyRegistryInfo(name: string | Observable<string>): Observable<QueryResult<any>> {
        return this.usergroupFactory.getCompanyRegistryInfo(name);
    }

    getCompensationRuleTemplates() {
        return of([{
            accepts_receipt: true,
            automatic_reload_amount: 50,
            categories: [
                {name: 'COMPETITIONS'},
                {name: 'GOODS'},
                {name: 'GUIDANCE'},
                {name: 'MEDICAL'},
                {name: 'OTHER'},
                {name: 'RECOVERY'},
                {name: 'WORKOUT'},
            ],
            compensation_percentage: 100,
            compensation_period: 1,
            credit_type: 'LIMITED',
            id: 0,
            max_compensation: 50,
            tax_exception_only: false,
        }]);
    }

    getEmployees() {
        return of([
            {
                email: 'mikk.maasikas@example.com',
                hash: '72600ba006cbb692e22f3b71c56e069d',
                id: 4315,
                pin: null,
                profile: {
                    first_name: 'Mikk',
                    last_name: 'Maasikas',
                },
            },
            {
                email: 'mari.maasikas@example.com',
                hash: '72600ba006c56dfggsfd2471c56e069d',
                id: 456,
                pin: null,
                profile: {
                    first_name: 'Mari',
                    last_name: 'Maasikas',
                },
            },
        ]);
    }

    getServiceCategories() {
        return of([
            {name: 'GOODS'},
            {name: 'OTHER'},
            {name: 'MEDICAL'},
            {name: 'WORKOUT'},
            {name: 'GUIDANCE'},
            {name: 'RECOVERY'},
            {name: 'COMPETITIONS'},
        ]);
    }
}
