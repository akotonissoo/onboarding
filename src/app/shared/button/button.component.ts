import { Component } from '@angular/core';

@Component({
    // tslint:disable
    selector: `
        button[sid-button], button[sid-button-raised], button[sid-button-primary], button[sid-button-secondary], button[sid-button-warn],  button[sid-button-small], button[sid-button-navbar], button[sid-button-stroked],
        a[sid-button], a[sid-button-raised], a[sid-button-primary], a[sid-button-secondary], a[sid-button-warn], a[sid-button-small], a[sid-button-stroked],
    `,
    // tslint:enable
    styleUrls: ['./button.component.scss'],
    templateUrl: './button.component.html',
})
export class ButtonComponent {}
