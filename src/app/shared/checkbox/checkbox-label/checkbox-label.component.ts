import { Component } from '@angular/core';

@Component({
    selector: 'sid-checkbox-label, [sid-checkbox-label]',
    styleUrls: ['./checkbox-label.component.scss'],
    templateUrl: './checkbox-label.component.html',
})
export class CheckboxLabelComponent {}
