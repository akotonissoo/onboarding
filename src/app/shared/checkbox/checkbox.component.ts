import {
    ChangeDetectorRef,
    Component,
    ElementRef,
    EventEmitter,
    HostBinding,
    Input,
    OnDestroy,
    OnInit,
    Output,
    Renderer2,
    ViewChild,
} from '@angular/core';
import { AbstractControl } from '@angular/forms';
import { BooleanFieldValue } from 'app/decorators';
import { selfOrParentHasClass, selfOrParentIsEqualElement } from '../../helpers';

@Component({
    selector: 'sid-checkbox',
    styleUrls: ['./checkbox.component.scss'],
    providers: [],
    templateUrl: './checkbox.component.html',
})
export class CheckboxComponent implements OnInit, OnDestroy {

    @Input() @BooleanFieldValue() alwaysShowErrors: boolean = false;
    @Input() @HostBinding('class') checkboxStyle: string = 'default';
    @Input() label: string = '';
    @Input() labelSecondary: string = '';
    @Input() requiredLabel: string;
    @Input() @BooleanFieldValue() noSpacing: boolean = false;
    @Input() controlName: string;
    @Input() group: AbstractControl;

    @Input() tooltip: string;
    @Input() tooltipPosition: string = 'top';

    _disabled: boolean = false;
    @Input() @BooleanFieldValue()
    get disabled(): boolean {
        return this._disabled;
    }
    set disabled(value: boolean) {
        this._disabled = value;
        this.setFormControlDisabledStatus(value);
    }

    _checked: boolean;
    @Input() @BooleanFieldValue()
    get checked(): boolean {
        return this._checked;
    }
    set checked(value: boolean) {
        this.setChecked(value);
    }

    @Output() checkedChange = new EventEmitter();
    @ViewChild('tooltipRef', {static: false}) tooltipRef: ElementRef;

    formControl: AbstractControl;
    tooltipOpen: boolean = false;

    private listeners: Function[] = []; // tslint:disable-line

    constructor(
        private renderer: Renderer2,
        private changeDetectorRef: ChangeDetectorRef,
    ) {}

    ngOnInit() {
        this.listeners.push(this.renderer.listen('document', 'click', this.closeTooltip.bind(this)));
        this.populateFormControl();
        this.setFormControlDisabledStatus(this._disabled);
    }

    ngOnDestroy() {
        this.listeners.forEach(cb => cb());
    }

    handleChange() {
        this.checkedChange.emit(this.checked);
    }

    setChecked(value: boolean): void {
        if (value !== this._checked) {
            this._checked = value;
            this.setFormControlValue(value);
        }
    }

    toggleChecked(event: MouseEvent) {
        if (this.disabled || selfOrParentHasClass(event.target as Element, 'js-deny-value-setting', true)) {
            return;
        }
        this.checked = !this.checked;
        this.handleChange();
    }

    toggleTooltip(): void {
        this.tooltipOpen = !this.tooltipOpen;
    }

    private closeTooltip(event: any): void {
        if (!this.tooltipRef) { return; }
        if (selfOrParentIsEqualElement(event.target, this.tooltipRef.nativeElement, true)) { return; }
        this.tooltipOpen = false;
        this.changeDetectorRef.detectChanges();
    }


    private isFormControl(): boolean {
        this.populateFormControl();
        return !!this.formControl;
    }

    private populateFormControl(): void {
        if (!this.group || !this.controlName) {
            this.formControl = null;
            return;
        }
        this.formControl = this.group.get(this.controlName);
    }

    private setFormControlDisabledStatus(status: boolean): void {
        if (this.isFormControl()) {
            if (status) {
                this.formControl.disable();
            } else {
                this.formControl.enable();
            }
        }
    }

    private setFormControlValue(value: any): void {
        if (this.isFormControl()) {
            this.formControl.setValue(value);
        }
    }
}
