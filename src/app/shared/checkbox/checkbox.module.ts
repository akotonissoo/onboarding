import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';

import { PipesModule } from '../../pipes';
import { FormControlErrorsModule } from '../form-control-errors/form-control-errors.module';
import { CheckboxLabelComponent } from './checkbox-label';
import { CheckboxComponent } from './checkbox.component';

@NgModule({
    imports:      [ CommonModule, ReactiveFormsModule, FormControlErrorsModule, PipesModule ],
    declarations: [ CheckboxComponent, CheckboxLabelComponent ],
    exports:      [ CheckboxComponent, CheckboxLabelComponent ],
})
export class CheckboxModule {}
