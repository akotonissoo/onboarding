import { Component, HostBinding, OnDestroy, OnInit } from '@angular/core';

import { Subscription } from 'rxjs';

import { CookieNoticeService } from './cookie-notice.service';

@Component({
    selector: 'sid-cookie-notice',
    templateUrl: 'cookie-notice.component.html',
    styleUrls: ['./cookie-notice.component.scss'],
})
export class CookieNoticeComponent implements OnInit, OnDestroy {
    @HostBinding('class.hidden') isHidden: boolean = true;

    private subscriptions: Subscription[] = [];

    constructor(private cookieNoticeService: CookieNoticeService) {}

    ngOnInit() {
        this.subscriptions.push(
            this.cookieNoticeService.getIsVisibleObservable().subscribe(
                result => {
                    this.isHidden = !result;
                },
            ),
        );
    }

    ngOnDestroy() {
        this.subscriptions.forEach(subscription => subscription.unsubscribe());
    }
}
