import { CommonModule } from '@angular/common';
import { ModuleWithProviders, NgModule } from '@angular/core';

import { TranslateModule } from '@ngx-translate/core';

import { CookieNoticeComponent } from './cookie-notice.component';
import { CookieNoticeService } from './cookie-notice.service';

@NgModule({
    imports:        [ CommonModule, TranslateModule ],
    exports:        [ CookieNoticeComponent ],
    declarations:   [ CookieNoticeComponent ],
})
export class CookieNoticeModule {
    static forRoot(): ModuleWithProviders {
        return {
            ngModule: CookieNoticeModule,
            providers: [ CookieNoticeService ],
        };
    }
}
