import { Injectable } from '@angular/core';

import * as moment from 'moment';
import { BehaviorSubject, Observable } from 'rxjs';

import { BrowserStorageTypeEnum, BrowserStorageValue } from 'app/interfaces';
import { BrowserStorageService } from 'app/services';

@Injectable()
export class CookieNoticeService {

    private isVisible: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

    constructor(private browserStorageService: BrowserStorageService) {}

    getIsVisibleObservable(): Observable<boolean> {
        return this.isVisible.asObservable();
    }

    hideNotice(): void {
        this.isVisible.next(false);
        this.setCookie();
    }

    showNotice(): void {
        if (this.isNoticeAllowed()) {
            this.isVisible.next(true);
        }
    }

    private isNoticeAllowed(): boolean {
        return !this.browserStorageService.getItem('cookieNotice', BrowserStorageTypeEnum.LocalStorage);
    }

    private setCookie(): void {
        const value: BrowserStorageValue = {
            expiresAt: moment().add(5, 'years').unix(),
        };

        this.browserStorageService.setItem('cookieNotice', value, BrowserStorageTypeEnum.LocalStorage);
    }
}
