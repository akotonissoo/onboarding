import { Component } from '@angular/core';

@Component({
    selector: 'sid-error',
    styleUrls: ['./error.component.scss'],
    templateUrl: './error.component.html',
})
export class ErrorComponent {}
