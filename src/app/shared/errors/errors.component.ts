import { Component, Input } from '@angular/core';

@Component({
    selector: 'sid-errors',
    templateUrl: './errors.component.html',
})
export class ErrorsComponent {
    @Input() errors: string[] = [];
}
