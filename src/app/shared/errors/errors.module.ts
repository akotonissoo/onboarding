import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { TranslateModule } from '@ngx-translate/core';

import { ErrorComponent } from './error/error.component';
import { ErrorsComponent } from './errors.component';

@NgModule({
    imports: [ CommonModule, TranslateModule ],
    declarations: [ ErrorsComponent, ErrorComponent ],
    exports: [ ErrorsComponent, ErrorComponent ],
})
export class ErrorsModule {}
