import { Component, Input } from '@angular/core';

import { AbstractControl } from '@angular/forms';

@Component({
    selector: 'sid-form-control-errors',
    templateUrl: './form-control-errors.component.html',
})
export class FormControlErrorsComponent {
    @Input() formControlItem: AbstractControl;
    @Input() alwaysShowErrors: boolean;
    @Input() requiredLabel: string;
}
