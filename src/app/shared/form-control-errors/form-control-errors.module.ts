import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { TranslateModule } from '@ngx-translate/core';

import { ErrorsModule } from '../errors';
import { FormControlErrorsComponent } from './form-control-errors.component';

@NgModule({
    imports: [ CommonModule, TranslateModule, ErrorsModule ],
    declarations: [ FormControlErrorsComponent ],
    exports: [ FormControlErrorsComponent ],
})
export class FormControlErrorsModule {}
