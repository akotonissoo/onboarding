import {
    AfterViewInit,
    ChangeDetectorRef,
    Component,
    ContentChild,
    ElementRef,
    EventEmitter,
    HostBinding,
    Input,
    OnDestroy,
    Output,
    Renderer2,
    ViewChild,
} from '@angular/core';
import { AbstractControl } from '@angular/forms';

import { Subscription } from 'rxjs';

import { BooleanFieldValue } from 'app/decorators';
import { selfOrParentIsEqualElement } from 'app/helpers';
import { InputDirective } from './input/input.directive';

@Component({
    selector: 'sid-form-field',
    templateUrl: 'form-field-component.html',
    styleUrls: ['form-field.component.scss'],
})
export class FormFieldComponent implements AfterViewInit, OnDestroy {
    @Input() @HostBinding('class') fieldStyle: string = 'default';

    @Input() prefixClass: string;
    @Input() label: string;
    @Input() suffixClass: string;

    @Input() tooltip: string;
    @Input() tooltipPosition: string = 'top';

    @Input() requiredLabel: string;

    @Input() @BooleanFieldValue() alwaysShowErrors: boolean = false;
    @Input() @BooleanFieldValue() noMinMaxLabels: boolean = false;

    @Input() @BooleanFieldValue() readonly: boolean = false;
    @Input() @BooleanFieldValue() readonlyNoValue: boolean = false;

    @Input() typeaheadData: any[] = [];
    @Input() typeaheadLabelKey: string = 'label';

    @Output() typeaheadSelect = new EventEmitter();
    @Output() prefixSelect: EventEmitter<void> = new EventEmitter();
    @Output() suffixSelect: EventEmitter<void> = new EventEmitter();

    @ViewChild('tooltipRef', {static: false}) tooltipRef: ElementRef;
    @ContentChild(InputDirective, {static: false}) inputDirective: InputDirective;

    allowBlurTypeaheadClose: boolean = true;
    formControl: AbstractControl;
    inFocus: boolean = false;
    tooltipOpen: boolean = false;
    typeaheadOpen: boolean = false;
    type: string = 'text';

    max: number = null;
    min: number = null;
    maxLength: number = null;
    minLength: number = null;

    private formComponent: ElementRef;
    private listeners: Function[] = []; // tslint:disable-line
    private subscriptions: Subscription[] = [];

    constructor(
        private renderer: Renderer2,
        private changeDetectorRef: ChangeDetectorRef,
    ) {
        this.listeners.push(renderer.listen('document', 'click', this.closeTooltip.bind(this)));
    }

    ngAfterViewInit() {
        if (this.readonly) {
            this.populateReadOnly();
        } else {
            this.populateNonReadOnly();
        }

        // As we repopulate some of the values in the lifecycle
        this.changeDetectorRef.detectChanges();
    }

    ngOnDestroy() {
        this.listeners.forEach(cb => cb());
        this.subscriptions.forEach(subscription => subscription.unsubscribe());
    }

    fetchFromObject(obj: any, prop: any): any {
        if (typeof obj === 'undefined') {
            return false;
        }

        const index = prop.indexOf('.');
        if (index > -1) {
            return this.fetchFromObject(obj[prop.substring(0, index)], prop.substr(index + 1));
        }

        return obj[prop];
    }

    mouseEnter(): void {
        this.allowBlurTypeaheadClose = false;
    }

    mouseLeave(): void {
        this.allowBlurTypeaheadClose = true;
    }

    prefixClick(): void {
        this.prefixSelect.emit();
    }

    suffixClick(): void {
        this.suffixSelect.emit();
    }

    toggleTooltip(): void {
        this.tooltipOpen = !this.tooltipOpen;
    }

    typeaheadItemSelect(typeaheadItem: MouseEvent): void {
        this.closeTypeahead();
        this.typeaheadSelect.emit(typeaheadItem);
    }

    private closeTooltip(event: any): void {
        if (!this.tooltipRef) { return; }
        if (selfOrParentIsEqualElement(event.target, this.tooltipRef.nativeElement, true)) { return; }
        this.tooltipOpen = false;
        this.changeDetectorRef.detectChanges();
    }

    private closeTypeahead(): void {
        this.allowBlurTypeaheadClose = true;
        this.typeaheadOpen = false;
    }

    private getNumberOrNull(numberLike: number): number {
        if (numberLike || numberLike === 0) {
            return numberLike;
        }

        return null;
    }

    private handleFocus(): void {
        this.inFocus = true;
    }

    private handleFocusOut(): void {
        this.inFocus = false;
    }

    private handleKeyDown(event: KeyboardEvent): void {
        this.openTypeahead();
        if (this.isTabKey(event)) {
            this.closeTypeahead();
        }
    }
    private handleKeyUp(event: KeyboardEvent): void {
        if (!this.isTabKey(event)) {
            this.openTypeahead();
        }
    }

    private isTabKey(keyEvent: KeyboardEvent): boolean {
        return keyEvent.keyCode === 9;
    }

    private openTypeahead(): void {
        this.typeaheadOpen = true;
    }

    private populateFormControlFields(): void {
        this.formControl = this.inputDirective.formGroup.get(this.inputDirective.formControlName);
    }

    private populateFormComponent(): void {
        this.formComponent = this.inputDirective.elementRef;

    }

    private populateFormComponentFocusEvents(): void {
        this.listeners.push(
            this.renderer.listen(
                this.formComponent.nativeElement,
                'focus',
                () => this.handleFocus(),
            ),

            this.renderer.listen(
                this.formComponent.nativeElement,
                'focusout',
                () => this.handleFocusOut(),
            ),
        );
    }

    private populateFormComponentKeyboardEvents(): void {
        this.listeners.push(
            this.renderer.listen(
                this.formComponent.nativeElement,
                'keydown',
                event => this.handleKeyDown(event),
            ),

            this.renderer.listen(
                this.formComponent.nativeElement,
                'keyup',
                event => this.handleKeyUp(event),
            ),
        );
    }

    private populateFormRangeLabels(): void {
        this.min = this.getNumberOrNull(this.formComponent.nativeElement.min);
        this.max = this.getNumberOrNull(this.formComponent.nativeElement.max);

        this.minLength = this.formComponent.nativeElement.minLength;
        this.maxLength = this.formComponent.nativeElement.maxLength;
    }

    private populateFormSubscriptions(): void {
        this.subscriptions.push(
            this.inputDirective.maxChange.subscribe(max => this.max = max),
            this.inputDirective.minChange.subscribe(min => this.min = min),
        );
    }

    private populateNonReadOnly(): void {
        this.populateFormControlFields();
        this.populateFormComponent();

        this.populateFormSubscriptions();
        this.populateFormComponentKeyboardEvents();
        this.populateFormComponentFocusEvents();
        this.populateFormRangeLabels();
    }

    private populateReadOnly(): void {
        /**/
    }
}

