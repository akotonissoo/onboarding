import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { TranslateModule } from '@ngx-translate/core';

import { FormControlErrorsModule } from '../form-control-errors/form-control-errors.module';
import { FormFieldComponent } from './form-field.component';
import { InputDirective } from './input/input.directive';
import { NumberDirective } from './number/number.directive';

@NgModule({
    imports: [CommonModule, FormsModule, TranslateModule, FormControlErrorsModule],
    exports: [FormFieldComponent, InputDirective, NumberDirective],
    declarations: [FormFieldComponent, InputDirective, NumberDirective],
})
export class FormFieldModule {}
