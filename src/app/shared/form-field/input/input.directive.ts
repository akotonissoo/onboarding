import { getLocaleNumberSymbol, NumberSymbol } from '@angular/common';
import {
    Directive,
    ElementRef,
    EventEmitter,
    Input,
    OnChanges,
    OnDestroy,
    OnInit,
    Output,
    SimpleChanges,
} from '@angular/core';
import { AbstractControl, FormGroup } from '@angular/forms';

import { Subscription } from 'rxjs';

import { env } from 'app/../../config/env';
import { BooleanFieldValue } from 'app/decorators';
import { FieldsRequiredException } from 'app/exceptions';

@Directive({
    selector: '[sidInput], [sidTextarea]',
})
export class InputDirective implements OnInit, OnChanges, OnDestroy {
    @Input() @BooleanFieldValue() readonly: boolean = false;
    @Input() formControlName: string;
    @Input() formGroup: FormGroup;
    @Input() max: number | string;
    @Input() min: number | string;

    @Output() maxChange: EventEmitter<number | string> = new EventEmitter();
    @Output() minChange: EventEmitter<number | string> = new EventEmitter();

    formControl: AbstractControl;

    private subscriptions: Subscription[] = [];

    constructor(
        public elementRef: ElementRef,
    ) {}

    ngOnInit() {
        this.populateFormControl();
        this.populateFormControlValueListener();
    }

    ngOnChanges(changes: SimpleChanges): void {
        if (changes.max) {
            this.maxChange.emit(changes.max.currentValue);
        }

        if (changes.min) {
            this.minChange.emit(changes.min.currentValue);
        }
    }

    ngOnDestroy() {
        this.subscriptions.forEach(subscription => subscription.unsubscribe());
    }

    private populateFormControl(): void {
        if (!this.formGroup || !this.formControlName) {
            throw new FieldsRequiredException(['formGroup', 'formControlName']);
        }

        this.formControl = this.formGroup.get(this.formControlName);
    }

    private populateFormControlValueListener() {
        this.subscriptions.push(
            this.formControl.valueChanges.subscribe(value => this.handleValueChange(value)),
        );
    }

    private handleValueChange(value: any): void {
        const locale = env.currencyLocale.locale;
        const separator = getLocaleNumberSymbol(locale ? locale : 'et-EE', NumberSymbol.CurrencyDecimal);

        if (this.min) {
            const minString = (this.min as string).replace(/\s/g,''); // tslint:disable-line
            const min = separator === ',' ? parseFloat(minString.replace(',', '.')) : parseFloat(minString);

            if (!isNaN(min) && value < min) {
                this.formControl.patchValue(min);
            }
        }

        if (this.max) {
            const maxString = (this.max as string).replace(/\s/g,''); // tslint:disable-line
            const max = separator === ',' ? parseFloat(maxString.replace(',', '.')) : parseFloat(maxString);

            if (!isNaN(max) && value > max) {
                this.formControl.patchValue(max);
            }
        }
    }
}
