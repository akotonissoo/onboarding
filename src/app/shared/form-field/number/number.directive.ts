import {
    Directive,
    ElementRef,
    EventEmitter,
    HostListener,
    Input,
    OnDestroy,
    OnInit,
    Output,
    Renderer2,
} from '@angular/core';
import { AbstractControl, FormGroup, NgModel } from '@angular/forms';

import { Subscription } from 'rxjs';

import { FieldsRequiredException } from 'app/exceptions';

@Directive({
    selector: '[sidInput][type="number"]',
    providers: [
        NgModel,
    ],
})
export class NumberDirective implements OnInit, OnDestroy {
    @Input() decimals: number;
    @Input() formGroup: FormGroup;
    @Input() formControlName;

    @Output() ngModelChange: EventEmitter<any> = new EventEmitter();

    // tslint:disable
    private JANITOR_REGEXP = /[^\d\-,.]/g; // non digits
    private FLOAT_REGEXP_1 = /^\$?-?(\d{1,3}(\.\d{3})*)(?=,)(,\d*)?$|^\$?(\d{1,3}(\.\d{3}){2,})(?=[^,])?(,\d*)?$/; // Numbers like: 1.123,56
    private FLOAT_REGEXP_2 = /^\$?-?(\d{1,3}(,\d{3})*)(?=\.)(\.\d*)?$|^\$?(\d{1,3}(,\d{3}){2,})(?=[^.])?(\.\d*)?$/; // Numbers like: 1,123.56
    private FLOAT_REGEXP_3 = /^\$?-?\d+(\.\d*)?$/; //Numbers like: 1123.56
    private FLOAT_REGEXP_4 = /^\$?-?\d+(,\d*)?$/; //Numbers like: 1123,56
    // tslint:enable

    private formControl: AbstractControl;
    private subscriptions: Subscription[] = [];

    constructor(elementRef: ElementRef, renderer: Renderer2) {
        renderer.setAttribute(elementRef.nativeElement, 'type', 'text');
    }

    ngOnInit() {
        this.populateFormControl();
        this.subscriptions.push(
            this.formControl.valueChanges.subscribe(value => this.handleValueChange(value)),
        );
    }

    ngOnDestroy() {
        this.subscriptions.forEach(subscription => subscription.unsubscribe());
    }

    @HostListener('focusout', ['$event'])
    onInputBlur(event: Event) {
        const eventTarget = event.target as HTMLInputElement;

        let newNumber: any = this.getNumber(eventTarget.value);

        if (newNumber) {
            newNumber = this.formatNumber(newNumber);
        }

        newNumber = (newNumber || newNumber === 0) && !isNaN(newNumber) ? parseFloat(newNumber) : null;

        this.formGroup.get(this.formControlName).patchValue(newNumber);
        this.ngModelChange.emit(newNumber);
    }

    private decimalPlaces(num: any): number {
        const match = ('' + num).match(/(?:\.(\d+))?(?:[eE]([+-]?\d+))?$/);
        if (!match) return 0;
        return Math.max(0, (match[1] ? match[1].length : 0) - (match[2] ? +match[2] : 0));
    }

    private formatNumber(num: any, decimals?: any): string {
        if (decimals == undefined) { // tslint:disable-line
            if (this.decimals == undefined) { // tslint:disable-line
                decimals = this.decimalPlaces(num);
            } else {
                decimals = this.decimals;
            }
        }

        if (isNaN(decimals) || decimals === undefined) {
            return parseFloat(num).toString();
        }
        return parseFloat(num).toFixed(decimals);
    }

    private getNumber(num: any, decimals?: any): string {
        let retNumber: any;

        if (!isNaN(num)) {
            num = num.toString();
        }

        num = num.replace(this.JANITOR_REGEXP, '');

        if (decimals == undefined) { // tslint:disable-line
            if (this.decimals == undefined) { // tslint:disable-line
                decimals  = this.decimalPlaces(num);
            } else {
                decimals = this.decimals;
            }
        }

        if (this.FLOAT_REGEXP_1.test(num)) {
            retNumber = parseFloat(num.replace(/\./g, '').replace(/,/g, '.'));

        } else if (this.FLOAT_REGEXP_2.test(num)) {
            retNumber = parseFloat(num.replace(/,/g, ''));

        } else if (this.FLOAT_REGEXP_3.test(num)) {
            retNumber = parseFloat(num);

        } else if (this.FLOAT_REGEXP_4.test(num)) {
            retNumber = parseFloat(num.replace(/,/g, '.'));

        } else {
            retNumber = null;
        }

        if (retNumber && decimals !== undefined && decimals !== this.decimalPlaces(num)) {
            retNumber = retNumber.toFixed(decimals);
        }
        return retNumber;
    }

    private handleValueChange(value: any): void {
        if (this.decimals === 0) {
            const newVal = parseInt(value, 10);
            if (!isNaN(newVal) && value !== newVal) {
                this.formControl.patchValue(newVal);
            }
        }
    }

    private populateFormControl(): void {
        if (!this.formGroup || !this.formControlName) {
            throw new FieldsRequiredException(['formGroup', 'formControlName']);
        }

        this.formControl = this.formGroup.get(this.formControlName);
    }
}
