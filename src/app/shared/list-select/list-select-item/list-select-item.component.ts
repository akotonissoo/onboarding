import { Component, EventEmitter, HostBinding, HostListener, Input, Output } from '@angular/core';

import { BooleanFieldValue } from 'app/decorators';

@Component({
    selector: 'sid-list-select-item, [sid-list-select-item]',
    templateUrl: './list-select-item.component.html',
    styleUrls: ['./list-select-item.component.scss'],
})
export class ListSelectItemComponent {
    @Input() label: string;
    @Input() @BooleanFieldValue() @HostBinding('class.selected') selected: boolean = false;

    @Output() selectedChange: EventEmitter<boolean> = new EventEmitter();

    @HostListener('click', ['$event'])
    onClick() {
        this.selectedChange.emit(!this.selected);
    }
}
