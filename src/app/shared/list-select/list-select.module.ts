import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { TranslateModule } from '@ngx-translate/core';

import { ListSelectItemComponent } from './list-select-item';
import { ListSelectComponent } from './list-select.component';

@NgModule({
    imports: [ CommonModule, TranslateModule ],
    declarations: [ ListSelectComponent, ListSelectItemComponent ],
    exports: [ ListSelectComponent, ListSelectItemComponent ],
})
export class ListSelectModule {}
