import { Component, OnInit } from '@angular/core';

import { LoadingBarService } from './loading-bar.service';

import { LoadingBarEventType } from './loading-bar-event.type';
import { LoadingBarEvent } from './loading-bar.event';

@Component({
    selector: 'sid-loading-bar',
    templateUrl: './loading-bar.component.html',
    styleUrls: [ './loading-bar.component.scss' ],
})
export class LoadingBarComponent implements OnInit {

    visible = false;

    constructor(private loadingBarService: LoadingBarService) {}

    ngOnInit() {

        this.loadingBarService.events.subscribe((event: LoadingBarEvent) => {
            switch (event.type) {

                case LoadingBarEventType.VISIBLE:
                    this.visible = event.value;
                    break;

                default:
                    break;
            }
        });

    }

}
