import { LoadingBarEventType } from './loading-bar-event.type';

export class LoadingBarEvent {

    constructor(public type: LoadingBarEventType, public value: any) {}

}
