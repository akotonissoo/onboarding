import { CommonModule } from '@angular/common';
import { ModuleWithProviders, NgModule } from '@angular/core';
import { MatProgressBarModule } from '@angular/material/progress-bar';

import { LoadingBarComponent } from './loading-bar.component';
import { LoadingBarService } from './loading-bar.service';

@NgModule({
    imports: [ CommonModule, MatProgressBarModule ],
    declarations: [ LoadingBarComponent ],
    exports: [ LoadingBarComponent ],
})
export class LoadingBarModule {

    static forRoot(): ModuleWithProviders {
        return {
            ngModule: LoadingBarModule,
            providers: [ LoadingBarService ],
        };
    }

}
