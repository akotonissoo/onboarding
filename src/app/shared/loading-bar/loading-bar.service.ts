import { EventEmitter, Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

import { LoadingBarEventType } from './loading-bar-event.type';
import { LoadingBarEvent } from './loading-bar.event';

@Injectable()
export class LoadingBarService {

    events: EventEmitter<LoadingBarEvent> = new EventEmitter<LoadingBarEvent>();

    private requests: BehaviorSubject<number> = new BehaviorSubject<number>(0);

    constructor() {
        this.requests.subscribe(counter => {
            if (counter === 0) {
                this.hideLoadingBar();
            }
            if (counter > 0) {
                this.showLoadingBar();
            }
        });
    }

    start(counter: number = 1) {
        this.requests.next(this.requests.value + counter);
    }

    finish(counter: number = 1) {
        this.requests.next(Math.max(this.requests.value - counter, 0));
    }

    showLoadingBar() {
        this.events.emit(new LoadingBarEvent(LoadingBarEventType.VISIBLE, true));
    }

    hideLoadingBar() {
        // A small window if more requests appear
        setTimeout(() => {
            if (this.requests.value === 0) {
                this.events.emit(new LoadingBarEvent(LoadingBarEventType.VISIBLE, false));
            }
        }, 250);
    }

}
