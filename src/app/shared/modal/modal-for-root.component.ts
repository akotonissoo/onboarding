import { Component, OnDestroy, OnInit } from '@angular/core';

import { Subscription } from 'rxjs';

import { ModalInterface } from './modal-for-root.interface';
import { ModalService } from './modal.service';


@Component({
    selector: 'sid-modal-for-root',
    styleUrls: ['./modal-for-root.component.scss'],
    templateUrl: './modal-for-root.component.html',
})
export class ModalForRootComponent implements OnInit, OnDestroy {

    modal: ModalInterface = {};
    modalOpen: boolean;

    private subscriptions: Subscription[] = [];

    constructor(
        private modalService: ModalService,
    ) {}

    ngOnInit() {
        this.subscriptions.push(
            this.modalService.getModalObservable().subscribe(
                (result) => {
                    this.modal = result;
                },
            ),
            this.modalService.getModalOpenObservable().subscribe(
                (result) => {
                    this.modalOpen = result;
                },
            ),
        );
    }

    ngOnDestroy() {
        this.subscriptions.forEach(subscription => subscription.unsubscribe());
    }

    modalOpenChange(isOpen) {
        isOpen ? this.modalService.openModal() : this.modalService.closeModal();
    }
}
