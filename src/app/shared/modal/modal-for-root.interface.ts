export interface ModalInterface {
    title?: string;
    titleParams?: Params;
    content?: string;
    contentParams?: Params;
    buttons?: ButtonInterface[];
    denyClose?: boolean;
}

interface Params {
    [key: string]: number | string;
}

export interface ButtonInterface {
    classes?: string;
    attributes?: {[key: string]: string | number};
    text: string;
    callback?(...rest): any;
}
