import { CommonModule } from '@angular/common';
import { ModuleWithProviders, NgModule } from '@angular/core';

import { TranslateModule } from '@ngx-translate/core';

import { DirectivesModule } from 'app/directives/directives.module';
import { PipesModule } from 'app/pipes';
import { ButtonModule } from '../button';
import { ModalComponent, ModalContentComponent, ModalCtaComponent, ModalTitleComponent } from './modal';
import { ModalForRootComponent } from './modal-for-root.component';
import { ModalService } from './modal.service';

@NgModule({
    imports: [ CommonModule, TranslateModule, PipesModule, ButtonModule, DirectivesModule ],
    declarations: [
        ModalForRootComponent,
        ModalComponent,
        ModalContentComponent,
        ModalCtaComponent,
        ModalTitleComponent,
    ],
    exports: [
        ModalForRootComponent,
        ModalComponent,
        ModalContentComponent,
        ModalCtaComponent,
        ModalTitleComponent,
    ],
})
export class ModalModule {
    static forRoot(): ModuleWithProviders {
        return {
            ngModule: ModalModule,
            providers: [ ModalService ],
        };
    }
}
