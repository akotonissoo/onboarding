import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

import { ModalInterface } from './modal-for-root.interface';

@Injectable()
export class ModalService {

    private modalOpen: Subject<boolean> = new Subject<boolean>();
    private modalSubject: Subject<ModalInterface> = new Subject<ModalInterface>();

    closeModal() {
        this.modalOpen.next(false);
        this.modalSubject.next({});
    }

    createModal(modal: ModalInterface) {
        this.modalSubject.next(modal);
        this.openModal();
    }

    getModalObservable() {
        return this.modalSubject.asObservable();
    }

    getModalOpenObservable() {
        return this.modalOpen.asObservable();
    }

    openModal() {
        this.modalOpen.next(true);
    }
}
