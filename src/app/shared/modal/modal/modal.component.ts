import { animate, group, query, style, transition, trigger } from '@angular/animations';
import {
    AfterViewInit,
    ChangeDetectionStrategy,
    ChangeDetectorRef,
    Component,
    ContentChildren,
    EventEmitter,
    Input,
    OnDestroy,
    Output,
    QueryList,
} from '@angular/core';
import { Subscription } from 'rxjs';

import { BooleanFieldValue } from 'app/decorators';

import { ModalContentComponent } from './modal-content';
import { ModalCtaComponent } from './modal-cta';
import { ModalTitleComponent } from './modal-title';

@Component({
    selector: 'sid-modal',
    styleUrls: ['./modal.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    templateUrl: './modal.component.html',
    animations: [
        trigger('fade', [
            transition(':enter', [
                // hide elems
                query(':self', style({opacity: 0})),
                query('.sid-modal', style({transform: 'scale(0.8)'})),

                // show elems
                group([
                    query(':self',
                        animate(200, style({
                            opacity: 1,
                        })),
                    ),
                    query('.sid-modal',
                        animate(150,
                            style({
                                transform: 'scale(1)',
                            }),
                        ),
                    ),
                ]),
            ]),
            transition(':leave', [
                query(':self',
                    animate(200, style({
                        opacity: 0,
                    })),
                ),
                query('.sid-modal',
                    animate(200, style({
                        transform: 'scale(0.8)',
                    })),
                ),
            ]),
        ]),
    ],
})
export class ModalComponent implements AfterViewInit, OnDestroy {
    @Input() @BooleanFieldValue() allowWide: boolean = false;
    @Input() @BooleanFieldValue() allowOverflow: boolean = false;
    @Input() denyClose: boolean = false;
    @Input() modalOpen: boolean = false;
    @Output() modalOpenChange: EventEmitter<boolean> = new EventEmitter<boolean>();

    @ContentChildren(ModalTitleComponent) modalTitle: QueryList<ModalTitleComponent>;
    @ContentChildren(ModalContentComponent) modalContent: QueryList<ModalContentComponent>;
    @ContentChildren(ModalCtaComponent) modalCta: QueryList<ModalCtaComponent>;
    modalTitleHasContent: boolean = false;
    modalContentHasContent: boolean = false;
    modalCtaHasContent: boolean = false;
    private childrenMap = new Map<QueryList<any>, any>();

    private subscriptions: Subscription[] = [];

    constructor(
        private changeDetectorRef: ChangeDetectorRef,
    ) {}

    ngAfterViewInit() {
        this.populateChildrenMaps();
        this.checkChildren();

        this.childrenMap.forEach((item, key) => {
            this.subscriptions.push(
                key.changes.subscribe(
                    changes => {
                        this.checkChild(changes);
                    },
                ),
            );
        });
    }

    ngOnDestroy() {
        this.subscriptions.forEach(subscription => subscription.unsubscribe());
    }

    closeModal(): void {
        this.modalOpen = false;
        this.modalOpenChanged();
        this.changeDetectorRef.markForCheck();
    }

    denyBubble(event: MouseEvent) {
        event.stopPropagation();
    }

    handleModalClose(): void {
        if (this.denyClose) {return; }
        this.closeModal();
    }

    modalOpenChanged(): void {
        this.modalOpenChange.emit(this.modalOpen);
    }

    openModal(): void {
        this.modalOpen = true;
        this.modalOpenChanged();
        this.changeDetectorRef.markForCheck();
    }

    private checkChild(child: QueryList<any>): void {
        this[this.childrenMap.get(child)] = !!child.length;
        this.changeDetectorRef.markForCheck();
    }

    private checkChildren(): void {
        this.childrenMap.forEach((item, key) => {
            this.checkChild(key);
        });
        this.changeDetectorRef.detectChanges();
    }
    private populateChildrenMaps(): void {
        this.childrenMap.set(this.modalTitle, 'modalTitleHasContent');
        this.childrenMap.set(this.modalContent, 'modalContentHasContent');
        this.childrenMap.set(this.modalCta, 'modalCtaHasContent');
    }
}
