import { Component, Input } from '@angular/core';

@Component({
    selector: 'sid-notice',
    styleUrls: ['./notice.component.scss'],
    templateUrl: './notice.component.html',
})
export class NoticeComponent {
    @Input() notice: string;
    @Input() noticeBold: string;
}
