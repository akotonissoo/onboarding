import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';

import { NoticeComponent } from './notice.component';

@NgModule({
    imports:      [ CommonModule, TranslateModule ],
    declarations: [ NoticeComponent ],
    exports:      [ NoticeComponent ],
})
export class NoticeModule {}
