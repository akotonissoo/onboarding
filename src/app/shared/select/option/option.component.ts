import { Component, ElementRef, Input, ViewChild } from '@angular/core';

import { BooleanFieldValue } from 'app/decorators';

@Component({
    selector: 'sid-option',
    templateUrl: './option.component.html',
})
export class OptionComponent {
    @Input() value: any;
    @Input() @BooleanFieldValue() placeholder: boolean = false;
    @Input() @BooleanFieldValue() selectsPlaceholder: boolean = false;

    @ViewChild('contentRef', {static: false}) contentRef: ElementRef;
}
