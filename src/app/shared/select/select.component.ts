import {
    AfterViewInit,
    ChangeDetectorRef,
    Component,
    ContentChildren,
    ElementRef,
    EventEmitter,
    HostBinding,
    Input,
    OnDestroy,
    OnInit,
    Output,
    QueryList,
    Renderer2,
    ViewChild,
} from '@angular/core';
import { AbstractControl } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';

import { BooleanFieldValue } from 'app/decorators';

import { selfOrParentIsEqualElement } from 'app/helpers';

import { OptionComponent } from './option';


@Component({
    selector: 'sid-select',
    styleUrls: ['./select.component.scss'],
    templateUrl: './select.component.html',
})
export class SelectComponent implements OnInit, AfterViewInit, OnDestroy {

    @Input() @HostBinding('class') selectStyle: string = 'default';

    @Input() label: string;

    private _value: string;
    @Input() set value(newValue: any) {
        this._value = newValue;
        this.handleOptions();
    }
    get value(): any {
        return this._value;
    }

    @Input() @BooleanFieldValue() alwaysShowErrors: boolean = false;
    @Input() @BooleanFieldValue() disabled: boolean;
    @Input() controlName: string;
    @Input() group: AbstractControl;
    @Input() optionsSortFunction: (itemA: any, itemB: any) => -1 | 0 | 1;

    @Output() valueChange = new EventEmitter();

    @ContentChildren(OptionComponent) optionsRefs: QueryList<OptionComponent>;
    @ViewChild('selectRef', {static: false}) selectRef: ElementRef;
    @ViewChild('selectOptionsRef', {static: false}) selectOptionsRef: ElementRef;

    dropdownVisible: boolean;
    formControl: AbstractControl;
    isPlaceholder: boolean = false;
    listeners: Function[] = []; // tslint:disable-line
    openDown: boolean = false;
    options: any[];
    prepareOpen: boolean = false;
    selectedLabel: string;

    private selectedOption: OptionComponent;
    private subscriptions: any[] = [];

    constructor(
        renderer: Renderer2,
        private changeDetectorRef: ChangeDetectorRef,
        private translateService: TranslateService,
    ) {
        this.listeners.push(
            renderer.listen('document', 'click', this.hideDropdown.bind(this)),
        );
    }

    ngOnInit() {
        this.populateFormControl();
        this.subscriptions.push(
            this.translateService.onLangChange.subscribe(() => {
                setTimeout(() => {
                    this.changeDetectorRef.detectChanges();
                    this.handleOptions();
                });
            }),
        );
    }

    ngAfterViewInit() {
        this.handleOptions();

        this.subscriptions.push(
            this.optionsRefs.changes.subscribe(() => this.handleOptions()),
        );
    }

    ngOnDestroy() {
        this.listeners.forEach(cb => cb());
        this.subscriptions.forEach(subscription => subscription.unsubscribe());
    }

    hideDropdown(event) {
        if (selfOrParentIsEqualElement(event.target, this.selectRef.nativeElement, true)) {return; }
        this.dropdownVisible = false;
        this.openDown = false;
        this.changeDetectorRef.detectChanges();
    }


    toggledropdown(): void {
        if (this.disabled) {return; }
        this.dropdownVisible = !this.dropdownVisible;
        if (this.dropdownVisible) {
            this.prepareOpen = true;
            this.openDown = false;
            // To render select before taking measurements
            this.changeDetectorRef.detectChanges();
            this.setOpeningDirection();
        }
    }

    private handleOptions(): void {
        if (!this.optionsRefs) {return; }

        this.optionsRefs.find(option => {
            if (option.value !== this.value) {
                return false;
            }

            this.onOptionSelect(option);
            return true;
        });

        let options = this.optionsRefs.reduce((results, option) => {
            if (option.placeholder && !option.selectsPlaceholder) {
                return results;
            }
            results.push({
                label: option.contentRef.nativeElement.innerHTML,
                value: option.value,
                cb: () => this.onOptionSelect(option),
            });

            return results;
        }, []);


        if (this.optionsSortFunction) {
            options = options.sort(this.optionsSortFunction);
        }

        this.options = options;

        this.changeDetectorRef.detectChanges();
    }

    private isFormControl(): boolean {
        return !!this.formControl;
    }

    private onOptionSelect(option: OptionComponent): void {
        this.setIsPlaceholderSelected(option.placeholder || false);
        this.setSelectedLabel(option.contentRef.nativeElement.innerHTML);
        this.selectValueChange(option);
    }

    private populateFormControl(): void {
        if (!this.group || !this.controlName) {
            this.formControl = null;
            return;
        }
        this.formControl = this.group.get(this.controlName);
    }

    private selectValueChange(option: OptionComponent): void {
        if (this.selectedOption && this.selectedOption.value === option.value) {return; }
        this.selectedOption = option;

        this.value = option.value;
        this.setFormControlValue(option.value);
        this.valueChange.emit(this.value);
    }

    private setFormControlValue(value: any): void {
        if (!this.isFormControl()) {return; }

        this.formControl.setValue(value);
    }

    private setIsPlaceholderSelected(isPlaceholder: boolean): void {
        this.isPlaceholder = isPlaceholder;
    }

    private setOpeningDirection(): void {
        this.prepareOpen = false;
        const selectRect = this.selectOptionsRef.nativeElement.getBoundingClientRect();
        const selectBottom = selectRect.bottom + selectRect.height + this.selectRef.nativeElement.clientHeight;

        this.openDown = window.innerHeight >= selectBottom;
    }

    private setSelectedLabel(label: string): any {
        this.selectedLabel = label;
    }
}
