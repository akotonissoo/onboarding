import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { FormControlErrorsModule } from '../form-control-errors/form-control-errors.module';
import { OptionComponent } from './option';
import { SelectComponent } from './select.component';

@NgModule({
    imports: [ CommonModule, FormControlErrorsModule ],
    declarations: [ SelectComponent, OptionComponent ],
    exports: [ SelectComponent, OptionComponent ],
})
export class SelectModule {}
