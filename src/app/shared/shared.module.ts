import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatRadioModule } from '@angular/material/radio';
import { MatSelectModule } from '@angular/material/select';

import { TranslateModule } from '@ngx-translate/core';
import { MomentModule } from 'ngx-moment';

import { ButtonModule } from './button';
import { CheckboxModule } from './checkbox';
import { CookieNoticeModule } from './cookie-notice';
import { ErrorsModule } from './errors';
import { FormFieldModule } from './form-field';
import { ListSelectModule } from './list-select';
import { LoadingBarModule } from './loading-bar';
import { ModalModule } from './modal';
import { NoticeModule } from './notice';
import { SelectModule } from './select';

const CORE = [ FormsModule, ReactiveFormsModule ];

const PACKAGES = [
    MomentModule,
    TranslateModule,

    MatFormFieldModule,
    MatInputModule,
    MatAutocompleteModule,
    MatInputModule,
    MatSelectModule,
    MatDatepickerModule,
    MatIconModule,
    MatRadioModule,
    MatProgressSpinnerModule,
];

const SPORTID = [
    ButtonModule,
    CheckboxModule,
    CookieNoticeModule,
    ErrorsModule,
    FormFieldModule,
    ListSelectModule,
    LoadingBarModule,
    ModalModule,
    NoticeModule,
    SelectModule,
];

@NgModule({
    exports: [
        ...CORE,
        ...SPORTID,
        ...PACKAGES,
    ],
})
export class SharedModule {}

