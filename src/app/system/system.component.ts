import { Component, OnDestroy, OnInit } from '@angular/core';

require('./system.file.scss'); // tslint:disable-line

@Component({
    templateUrl: './system.component.html',
})
export class SystemComponent implements OnInit, OnDestroy {
    ngOnInit() {
        document.querySelector('html').classList.add('app');
    }

    ngOnDestroy() {
        document.querySelector('html').classList.remove('app');
    }
}
