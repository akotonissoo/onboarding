// angular
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { SharedModule } from 'app/shared';
import { SystemComponent } from './system.component';
import { ROUTES } from './system.routes';

@NgModule({
    imports: [
        ROUTES,
        CommonModule,
        SharedModule,
    ],
    declarations: [ SystemComponent ],
})
export class SystemModule {}
