import { ModuleWithProviders } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { SystemComponent } from './system.component';

export const routes: Routes = [
    {
        path: '',
        component: SystemComponent,
        children: [
            {
                path: '',
                loadChildren: '../public/public.module#PublicModule',
            },
        ],
    },
];

export const ROUTES: ModuleWithProviders = RouterModule.forChild(routes);
