module.exports = {
    'files': [
        './*.svg'
    ],
    'fontName': 'sportid-icons',
    'classPrefix': 'sico-',
    'baseSelector': '.sico',
    'types': ['eot', 'woff', 'ttf', 'svg'],
    'fileName': 'sportid-font/[fontname].[chunkhash].[ext]'
};
