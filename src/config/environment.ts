import { ApplicationRef, enableProdMode } from '@angular/core';
import {
    disableDebugTools,
    enableDebugTools,
} from '@angular/platform-browser';

let _environmentDecorator = <T>(value: T): T => value;

if (process.env.NODE_ENV === 'production') {
    enableProdMode();
    loadProductionScripts();
    disableDebugTools();
} else {
    _environmentDecorator = (modRef: any) => {
        const appRef = modRef.injector.get(ApplicationRef);
        const cmpRef = appRef.components[0];

        const _ng = (window as any).ng;
        enableDebugTools(cmpRef);
        (window as any).ng.probe = _ng.probe;
        (window as any).ng.coreTokens = _ng.coreTokens;
        return modRef;
    };
}

function loadProductionScripts() {
    const node = document.createElement('script');
    node.src = 'assets/js/production.js';
    node.type = 'text/javascript';
    node.async = true;
    node.charset = 'utf-8';
    document.getElementsByTagName('head')[0].appendChild(node);
}

export const environmentDecorator = _environmentDecorator;
