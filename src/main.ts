require('./assets/fonts/sportid-icon/sportid-icon.font.js'); // tslint:disable-line

import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { environmentDecorator } from './config/environment';

import { AppModule } from './app';

platformBrowserDynamic()
    .bootstrapModule(AppModule)
    .then(environmentDecorator)
    .catch(err => console.error(err));
