// tslint:disable:ordered-imports
// Polyfills

// custom polyfills
import './polyfills';

import 'core-js/es';

import 'core-js/proposals/reflect-metadata';
import 'core-js/es/object';
import 'core-js/es/array';
import 'zone.js/dist/zone';

// Typescript emit helpers polyfill
import 'ts-helpers';

if (process.env.NODE_ENV === 'production') {
    // TODO: ?
}

if (process.env.NODE_ENV === 'development' || process.env.NODE_ENV === 'test') {
    // Error.stackTraceLimit = Infinity;
    // Error.stackTraceLimit = 30;
    // require('zone.js/dist/long-stack-trace-zone');
}
