((arr) => {
  arr.forEach((item) => {
    if (item.hasOwnProperty('remove')) {
      return;
    }
    Object.defineProperty(item, 'remove', {
      configurable: true,
      enumerable: true,
      writable: true,
      value: function remove() {
        this.parentNode.removeChild(this); // tslint:disable-line
      },
    });
  });
})([Element.prototype, CharacterData.prototype, DocumentType.prototype]);
