if (!window.location.origin) {
    (window.location.origin as any) =
        window.location.protocol +
        '//' + window.location.hostname +
        (window.location.port ? ':' + window.location.port : '');
}
